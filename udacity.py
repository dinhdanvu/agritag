import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from imgaug import augmenters as iaa
from keras.models import Sequential
import tensorflow as tf
from keras.layers import Convolution2D, Dense, MaxPooling2D, Dropout, Flatten
import cv2
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import pandas as pd
import random
import ntpath

print('Setting UP')
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def getName(filePath):
    return filePath.split('\\')[-1]


def importDataInfo(path):
    columns = ['Center', 'Left', 'Right', 'Steering', 'Throttle', 'Brake', 'Speed']
    data = pd.read_csv(os.path.join(path, 'driving_log.csv'), names=columns)
    # REMOVE FILE PATH AND GET ONLY FILE NAME
    # print(getName(data['center'][0]))
    data['Center'] = data['Center'].apply(getName)
    # print(data.head())
    print('Total Images Imported', data.shape[0])
    return data


path = 'myData'
data = importDataInfo(path)


def balanceData(data, display=True):
    nbins = 31
    samplesPerBin = 500
    hist, bins = np.histogram(data['Steering'], nbins)
    # print(bins)
    if display:
        center = (bins[:-1] + bins[1:]) * 0.5

        # print(center)
        plt.bar(center, hist, width=0.06)
        plt.plot((-1, 1), (samplesPerBin, samplesPerBin))
        # plt.show()

    removeIndexList = []
    for j in range(nbins):
        binDataList = []
        for i in range(len(data['Steering'])):
            if data['Steering'][i] >= bins[j] and data['Steering'][i] <= bins[j + 1]:
                binDataList.append(i)
        binDataList = shuffle(binDataList)
        binDataList = binDataList[samplesPerBin:]
        removeIndexList.extend(binDataList)
    #   print('Removed Images:',len(removeIndexList))
    data.drop(data.index[removeIndexList], inplace=True)
    #  print('Remaining Images:', len(data))

    if display:
        hist, _ = np.histogram(data['Steering'], nbins)
        plt.bar(center, hist, width=0.06)
        plt.plot((-1, 1), (samplesPerBin, samplesPerBin))
    #    plt.show()

    return data


balanceData(data, display=False)


def loadData(path, data):
    imagesPath = []
    steering = []
    for i in range(len(data)):
        indexed_data = data.iloc[i]
        imagesPath.append(f'{path}/IMG/{indexed_data[0]}')
        steering.append(float(indexed_data[3]))
    imagesPath = np.asarray(imagesPath)
    steering = np.asarray(steering)
    return imagesPath, steering


imagesPath, steerings = loadData(path, data)

xTrain, xVal, yTrain, yVal = train_test_split(imagesPath, steerings, test_size=0.2, random_state=10)
print('Total Training Images: ', len(xTrain))
print('Total Validation Images: ', len(xVal))


def augmentImage(imgPath, steering):
    img = mpimg.imread(imgPath)
    if np.random.rand() < 0.5:
        pan = iaa.Affine(translate_percent={'x': (-0.1, -0.1), 'y': (-0.1, -0.1)})
        img = pan.augment_image(img)

    if np.random.rand() < 0.5:
        zoom = iaa.Affine(scale=(1, 1.2))
        img = zoom.augment_image(img)

    if np.random.rand() < 0.5:
        brightness = iaa.Multiply((0.4, 1.2))
        img = brightness.augment_image(img)

    if np.random.rand() < 0.5:
        img = cv2.flip(img, 1)
        steering = -steering

    return img, steering


def preProcessing(img):
    img = img[60:135, :, :]
    img = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
    img = cv2.GaussianBlur(img, (3, 3), 0)
    img = cv2.resize(img, (200, 66))
    img = img / 255
    return img


def batchGen(_imagesPath, steeringList, batchSize, trainFlag):
    while True:
        imgBatch = []
        steeringBatch = []

        for i in range(batchSize):
            index = random.randint(0, len(_imagesPath) - 1)
            if trainFlag:
                img, steering = augmentImage(_imagesPath[index], steeringList[index])
            else:
                img = mpimg.imread(_imagesPath[index])
                steering = steeringList[index]
            img = preProcessing(img)
            imgBatch.append(img)
            steeringBatch.append(steering)
        yield np.asarray(imgBatch), np.asarray(steeringBatch)


def createModel():
    model = Sequential()

    model.add(Convolution2D(24, (5, 5), (2, 2), input_shape=(66, 200, 3), activation='elu'))
    model.add(Convolution2D(36, (5, 5), (2, 2), input_shape=(66, 200, 3), activation='elu'))
    model.add(Convolution2D(48, (5, 5), (2, 2), input_shape=(66, 200, 3), activation='elu'))
    model.add(Convolution2D(64, (3, 3), (1, 1), input_shape=(66, 200, 3), activation='elu'))
    model.add(Convolution2D(64, (3, 3), (1, 1), input_shape=(66, 200, 3), activation='elu'))

    model.add(Flatten())
    model.add(Dense(100, activation='elu'))
    model.add(Dense(50, activation='elu'))
    model.add(Dense(10, activation='elu'))
    model.add(Dense(1))

    model.compile(tf.keras.optimizers.Adam(lr=0.0001), loss='mse')

    return model


model = createModel()
model.summary()

history = model.fit(batchGen(xTrain, yTrain, 100, 3), steps_per_epoch=300, epochs=10,
                    validation_data=batchGen(xVal, yVal, 100, 0), validation_steps=200)

model.save('model.h5')
print('Model Saved')
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.legend(['Training', 'Validation'])
plt.ylim(0, 1)
plt.title('Loss')
plt.xlabel('Epoch')
plt.show()
