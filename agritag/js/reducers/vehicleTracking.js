'use strict';

var initialState = {
  notifications: [],
  notificationCount: 0,
  distanceToVehicle: '',
  durationToVehicle: '',
  vehicleList: [],
  vehiclesLite: [],
  findDevicePoints:[],
  mapType: 'standard',
};
function vehicleTracking(state = initialState, action) {
  switch (action.type) {
    case 'FIND_DEVICE_SUCCESS':
      return {
        ...state,
        findDevicePoints: action.points,
        distanceToVehicle: action.distanceToVehicle,
        durationToVehicle: action.durationToVehicle,
      };
    case 'FETCH_VEHICLE_SUCCESS':
      return {
        ...state,
        vehicleList: action.vehicleList,
        vehiclesLite: action.vehiclesLite
      };
    case 'UPDATE_VEHICLE_SUCCESS':
        return {
          ...state,
          vehicleList: action.vehicleList,
          vehiclesLite: action.vehiclesLite,
        };
    case 'LOAD_REVIEW_VEHICLE_SUCCESS':
        return {
          ...state,
          coordinates: action.coordinates,
          directions: action.directions,
          infos: action.infos
        };
    case 'CHANGE_MAP_TYPE':
      return {
        ...state,
        mapType: action.data,
      };
    default:
      return state;
  }
}
module.exports = vehicleTracking;
