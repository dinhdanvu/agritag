'use strict';

var initialState = {
  token: '',
  lang: 0,
  isLogin: false,
};
function dragonFly(state = initialState, action) {
  switch (action.type) {
    case 'SET_LANGUAGE':
      return {
        ...state,
        lang: action.data,
      };
      case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLogin: true,
        token: action.data,
      };
    case 'LOGOUT':
      return {
        ...state,
        isLogin: false,
      };
    case 'LOGIN_FAIL':
      return {
        ...state,
        isLogin: false,
      };
    default:
      return state;
  }
}
module.exports = dragonFly;
