'use strict';

import {combineReducers} from 'redux';

const appReducer = combineReducers({
  dragonFly: require('./dragonFly'),
  vehicleTracking: require('./vehicleTracking'),
});

const rootReducer = (state, action) => {
  if (action.type === 'RESET_APP') {
    const {dragonFly, vehicleTracking} = state;
    state = {dragonFly, vehicleTracking};
  }

  return appReducer(state, action);
};

export default rootReducer;
