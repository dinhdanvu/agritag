module.exports = {
  getImageSource(imageName) {
    switch (imageName) {
      case 'house1_color':
        return require('./house1_color.png');
      case 'house1_gray':
        return require('./house1_gray.png');
      case 'house2_color':
        return require('./house2_color.png');
      case 'house2_gray':
        return require('./house2_gray.png');
      case 'house3_color':
        return require('./house3_color.png');
      case 'house3_gray':
        return require('./house3_gray.png');
      case 'house4_color':
        return require('./house4_color.png');
      case 'house4_gray':
        return require('./house4_gray.png');
      case 'house5_color':
        return require('./house5_color.png');
      case 'house5_gray':
        return require('./house5_gray.png');
      case 'house6_color':
        return require('./house6_color.png');
      case 'house6_gray':
        return require('./house6_gray.png');
      case 'house7_color':
        return require('./house7_color.png');
      case 'house7_gray':
        return require('./house7_gray.png');
      case 'house8_color':
        return require('./house8_color.png');
      case 'house8_gray':
        return require('./house8_gray.png');
      case 'house9_color':
        return require('./house9_color.png');
      case 'house9_gray':
        return require('./house9_gray.png');
      case 'house10_color':
        return require('./house10_color.png');
      case 'house10_gray':
        return require('./house10_gray.png');
    }
  },
};
