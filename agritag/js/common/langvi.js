var langvi = {
  ERROR_PARSE_JSON: 'Lỗi xử lý dữ liệu',
  ERROR_NO_INTERNET: 'Không có kết nối Internet',

  error: {
    'Network Error': 'Không có kết nối Internet',
    'Request failed with status code 500': 'Lỗi Server',
    LOGIN_FAILED: 'Tài khoản/ Mật khẩu không chính xác',
    'Network request failed': 'Lỗi Server',
    SQL_ERROR: 'Dữ liệu bị lỗi, vui lòng kiểm tra lại',
  },
};

module.exports = langvi;
