/**
 * React Native App
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Image,
  Text,
  NativeModules,
  Platform,
  StatusBar,
  LogBox,
} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import LoadingView from './components/Common/ShowLoadingView';
import ImageUtil from './common/image/ImageUtil';
import globalSetting from './common/setting';
import ScaleUtils from './common/util/ScaleUtils';
import House1View from './components/Views/House1View';
import House6View from './components/Views/House6View';
import House7View from './components/Views/House7View';
import House8View from './components/Views/House8View';
import House9View from './components/Views/House9View';
import House10View from './components/Views/House10View';
import AboutView from './components/Views/AboutView';
import UserManualView from './components/Views/UserManualView';
import DetailView from './components/Views/DetailView';
LogBox.ignoreAllLogs(true);
class DragonFly extends Component {
  constructor(props) {
    super(props);
  }
  async showLoad() {
    this.loadingView.showLoadingView();
  }

  hideLoad() {
    this.loadingView.hideLoadingView();
  }
  render() {
    const TabNavigation = createBottomTabNavigator(
      {
        // house1: { screen: AboutView },
        // house2: { screen: UserManualView },
        // house3: { screen: AboutView },
        // house4: { screen: UserManualView },
        // house5: { screen: AboutView },
        house6: { screen: House6View },
        house7: { screen: House7View },
        house8: { screen: House8View },
        house9: { screen: House9View },
        house10: { screen: House10View },
      },
      {
        navigationOptions: ({ navigation }) => ({
          title:
          navigation.state.routeName,
          tabBarIcon: ({ focused, tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'house1') {
              iconName = `${focused ? 'house1_color' : 'house1_gray'}`;
            } else if (routeName === 'house2') {
              iconName = `${focused ? 'house2_color' : 'house2_gray'}`;
            } else if (routeName === 'house3') {
              iconName = `${focused ? 'house3_color' : 'house3_gray'}`;
            } else if (routeName === 'house4') {
              iconName = `${focused ? 'house4_color' : 'house4_gray'}`;
            } else if (routeName === 'house5') {
              iconName = `${focused ? 'house5_color' : 'house5_gray'}`;
            } else if (routeName === 'house6') {
              iconName = `${focused ? 'house6_color' : 'house6_gray'}`;
            } else if (routeName === 'house7') {
              iconName = `${focused ? 'house7_color' : 'house7_gray'}`;
            } else if (routeName === 'house8') {
              iconName = `${focused ? 'house8_color' : 'house8_gray'}`;
            } else if (routeName === 'house9') {
              iconName = `${focused ? 'house9_color' : 'house9_gray'}`;
            } else if (routeName === 'house10') {
              iconName = `${focused ? 'house10_color' : 'house10_gray'}`;
            } 
            return (
              <View>
                <Image
                  style={{
                    height: ScaleUtils.floorScale(30),
                    width: ScaleUtils.floorScale(30),
                    marginVertical: 10,
                  }}
                  source={ImageUtil.getImageSource(iconName)}
                  resizeMode="contain"
                />
                {/* <Text style={{position:'absolute',top:4,left:4}}>1</Text> */}
              </View>

            );
          },
          tabBarOnPress: ({ navigation, defaultHandler }) => {
            //console.log('onPress:', navigation.state.routes[0].routeName);
            //this.props.dispatch(changeTab(navigation.state.routes[0].routeName))
            defaultHandler();
          },
        }),
        tabBarOptions: {
          activeTintColor: globalSetting.mainColor,
          showLabel: true,
          style: {
            backgroundColor: globalSetting.main_gray_background,
            borderTopWidth: 2,
            height: ScaleUtils.floorScale(60),
            borderTopColor: '#E5E5E5',
          },
          activeTabStyle: {},
        },
      }
    );
    return (
      <View style={{ flex: 1 }}>
        <TabNavigation
          screenProps={{
            hideLoad: this.hideLoad.bind(this),
            showLoad: this.showLoad.bind(this),
          }}
        />
        <LoadingView
          ref={(input) => (this.loadingView = input)}
          isModal={true}
          timeout={5000}
        />
      </View>
    );
  }
}

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(DragonFly);
