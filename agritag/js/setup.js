'use strict';

import React from 'react';
import {Provider} from 'react-redux';
import {RootSiblingParent} from 'react-native-root-siblings';

var configureStore = require('./store/configureStore');
import DragonFly from './DragonFly';
class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      store: configureStore(() => this.setState({isLoading: false})),
    };
  }

  render() {
    if (this.state.isLoading) {
      return null;
    }
    return (
      <Provider store={this.state.store}>
        <RootSiblingParent>
          <DragonFly />
        </RootSiblingParent>
      </Provider>
    );
  }
}
module.exports = Root;
