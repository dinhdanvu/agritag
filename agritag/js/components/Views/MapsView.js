import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  StatusBar,
  Platform,
} from 'react-native';

import DrawerLayout from 'react-native-drawer-layout';
import ClusteredMapView from 'react-native-map-clustering';
import { Marker, Polyline, AnimatedRegion } from 'react-native-maps';
import { connect } from 'react-redux';
var HashMap = require('hashmap');
import Slider from 'react-native-slider';
import ImageUtil from '../../common/image/ImageUtil';
import globalSetting from '../../common/setting';
import HeaderCommon from '../Common/HeaderCommon';
import ShowLoadingView from '../Common/ShowLoadingView';
import HeaderStatus from '../Common/HeaderStatus';
import VehicleItem from '../Common/VehicleItem';
import VehicleDetail from '../Common/VehicleDetail';
import language from '../../common/language';
import PopupChooseDevice from '../Common/PopupChooseDevice';
import PopupCalendar from '../Common/PopupCalendar';
import PopupChangeMapType from '../Common/PopupChangeMapType';
import ScaleUtils from '../../common/util/ScaleUtils';
import { showToast, loadReviewRouteDevices } from '../../actions';
const myLocation = { latitude: 11.57071, longitude: 106.680199 };
class MapsView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      speed: 1,
      playing: false,
      INITIAL_REGION:
        global.display > 0
          ? {
              latitude: global.vehicle.latitude,
              longitude: global.vehicle.longitude,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01,
            }
          : {
              latitude: 16.02047,
              longitude: 105.79232,
              latitudeDelta: 11.935,
              longitudeDelta: 11.935,
            },
      showTrackingTime: false,
      coordinates: global.coordinates,
      sliderValue: 0,
      statusBarHeight: 0,
      isShowChooseDevice: false,
      isShowChangeMapType: false,
      isShowCalendar: false,
      showsUserLocation: true,
      groupedDevices: [],
      vehicle: global.vehicle,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // const { vehicle } = this.props.vehicleTracking;
    // const _vehicle = nextProps.vehicleTracking.vehicle;
    // const { vehicleList } = this.props.vehicleTracking;
    // const _vehicleList = nextProps.vehicleTracking.vehicleList;
    // if (
    //   vehicle.plate === _vehicle.plate &&
    //   (vehicle.latitude !== _vehicle.latitude ||
    //     vehicle.longitude !== _vehicle.longitude)
    // ) {
    //   this.mapView.animateCamera({
    //     center: {
    //       latitude: _vehicle.latitude,
    //       longitude: _vehicle.longitude,
    //     },
    //   });
    // }
    //todo
    // let driverWayCordAnim = [];
    // let map = new HashMap();
    // vehicleList.forEach((element) => {
    //   map.set(element.plate, element);
    // });
    // _vehicleList.forEach((element) => {
    //   const vehicle = map.get(element.plate);
    //   if (
    //     vehicle &&
    //     (vehicle.latitude !== element.latitude ||
    //       vehicle.longitude !== element.longitude)
    //   ) {
    //     driverWayCordAnim.push(element);
    //   }
    // });
    // driverWayCordAnim.forEach((element) => {
    //   this.carMoves[element.plate].animateMarkerToCoordinate({
    //     latitude: element.latitude,
    //     longitude: element.longitude,
    //   }, 500);
    // });
    //console.log(this.carMoves)
  }

  componentWillUnmount() {
    if (this.blurListener) {
      this.blurListener.remove();
    }
    if (this.listener) {
      this.listener.remove();
    }
  }

  componentDidMount() {
    this.blurListener = this.props.navigation.addListener('willBlur', () => {
      // console.log('blur');
      if (this.playInterval) {
        clearInterval(this.playInterval);
      }
    });
    this.listener = this.props.navigation.addListener('willFocus', () => {
      // console.log(global.display);
      // console.log(global.vehicle);

      if (global.display === 0) {
        this.setState({ showsUserLocation: false });
        if (global.mapReady) {
          this.mapView.animateToRegion(
            {
              latitude: 16.02047,
              longitude: 105.79232,
              latitudeDelta: 11.935,
              longitudeDelta: 11.935,
            },
            500
          );
        }
      } else if (global.display === 3) {
        this.setState({ vehicle: global.vehicle });
        setTimeout(() => {
          this.setState({
            statusBarHeight: 1,
            showsUserLocation: true,
            coordinates: global.coordinates,
          });
          this.mapView.fitToCoordinates(
            this.props.vehicleTracking.findDevicePoints,
            {
              edgePadding: {
                bottom: 200,
                right: 50,
                top: 150,
                left: 50,
              },
              animated: true,
            }
          );
        }, 500);
      } else if (global.display == 2) {
        this.setState({
          playing: false,
          step: 0,
          infos: global.infos,
          coordinates: global.coordinates,
          vehicle: global.vehicle,
          showsUserLocation: false,
          showTrackingTime: true,
        });
        if (global.mapReady) {
          this.mapView.animateToRegion(
            {
              latitude: global.vehicle.latitude,
              longitude: global.vehicle.longitude,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01,
            },
            500
          );
        }
        this.playInterval = setInterval(() => {
          // console.log(' run...');
          if (this.state.playing) {
            //go.
            let { step, speed } = this.state;

            if (step + speed < infos.length - 4) {
              this.setState({
                step: step + speed,
                vehicle: global.infos[step + speed],
              });
              let nextCoordinate = {
                latitude: global.infos[step + speed].latitude,
                longitude: global.infos[step + speed].longitude,
              };
              if (this.carReview) {
                if (Platform.OS == 'android') {
                  this.carReview.animateMarkerToCoordinate(nextCoordinate, 500);
                }
              }
              this.mapView.animateCamera({
                center: nextCoordinate,
              });
            } else {
              this.setState({ playing: false });
            }
          }
        }, 2000);
      } else if (global.display === 1) {
        this.setState({
          vehicle: global.vehicle,
          showsUserLocation: false,
        });
        if (global.mapReady) {
          this.mapView.animateToRegion(
            {
              latitude: global.vehicle.latitude,
              longitude: global.vehicle.longitude,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01,
            },
            500
          );
        }
      }
    });
  }

  onDateChange = (date) => {
    if (
      new Date().setHours(0, 0, 0, 0) >= new Date(date).setHours(0, 0, 0, 0)
    ) {
      this.loadingView.showLoadingView();
      this.props
        .dispatch(
          loadReviewRouteDevices(
            this.state.vehicle,
            new Date(date).setHours(0, 0, 0, 0)
          )
        )
        .then((res) => {
          console.log('success');
          this.loadingView.hideLoadingView();
          this.setState({
            playing: false,
            step: 0,
            infos: global.infos,
            coordinates: global.coordinates,
            vehicle: global.vehicle,
            showsUserLocation: false,
            showTrackingTime: true,
          });
          if (global.mapReady) {
            this.mapView.animateToRegion(
              {
                latitude: global.vehicle.latitude,
                longitude: global.vehicle.longitude,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
              },
              500
            );
          }
          this.playInterval = setInterval(() => {
            // console.log(' run...');
            if (this.state.playing) {
              //go.
              let { step, speed } = this.state;

              if (step + speed < infos.length - 4) {
                this.setState({
                  step: step + speed,
                  vehicle: global.infos[step + speed],
                });
                let nextCoordinate = {
                  latitude: global.infos[step + speed].latitude,
                  longitude: global.infos[step + speed].longitude,
                };
                if (this.carReview) {
                  if (Platform.OS == 'android') {
                    this.carReview.animateMarkerToCoordinate(
                      nextCoordinate,
                      500
                    );
                  }
                }
                this.mapView.animateCamera({
                  center: nextCoordinate,
                });
              } else {
                this.setState({ playing: false });
              }
            }
          }, 2000);
        })
        .catch((e) => {
          console.log(e);
          this.loadingView.hideLoadingView();
        });
    } else {
      console.log('not available');
    }
    this.setState({ isShowCalendar: false });
  };

  gotoDetail = () => {
    global.display = 0;
    global.vehicle = this.state.vehicle;
    this.props.navigation.navigate('detail');
  };

  gotoRoutInday = () => {
    if (this.playInterval) {
      clearInterval(this.playInterval);
    }
    this.loadingView.showLoadingView();
    this.props
      .dispatch(loadReviewRouteDevices(this.state.vehicle, null))
      .then((res) => {
        console.log('success');
        this.loadingView.hideLoadingView();
        this.setState({
          playing: false,
          step: 0,
          infos: global.infos,
          coordinates: global.coordinates,
          vehicle: global.vehicle,
          showsUserLocation: false,
          showTrackingTime: true,
        });
        if (global.mapReady) {
          this.mapView.animateToRegion(
            {
              latitude: global.vehicle.latitude,
              longitude: global.vehicle.longitude,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01,
            },
            500
          );
        }
        this.playInterval = setInterval(() => {
          // console.log(' run...');
          if (this.state.playing) {
            //go.
            let { step, speed } = this.state;

            if (step + speed < infos.length - 4) {
              this.setState({
                step: step + speed,
                vehicle: global.infos[step + speed],
              });
              let nextCoordinate = {
                latitude: global.infos[step + speed].latitude,
                longitude: global.infos[step + speed].longitude,
              };
              if (this.carReview) {
                if (Platform.OS == 'android') {
                  this.carReview.animateMarkerToCoordinate(nextCoordinate, 500);
                }
              }
              this.mapView.animateCamera({
                center: nextCoordinate,
              });
            } else {
              this.setState({ playing: false });
            }
          }
        }, 2000);
      })
      .catch((e) => {
        console.log(e);
        this.loadingView.hideLoadingView();
      });
  };

  playReview = () => {
    if (!this.state.playing) {
      console.log('play on');
      this.setState({
        playing: true,
      });
    } else {
      console.log('play off');
      this.setState({
        playing: false,
      });
    }
  };

  selectVehicleInGroup = (plate) => {
    const vehicle = this.props.vehicleTracking.vehicleList.find(
      (obj) => obj.plate === plate
    );
    this.setState({
      vehicle,
      isShowChooseDevice: false,
      showTrackingTime: false,
      showsUserLocation: false,
    });
    const initialRegion = {
      latitude: vehicle.latitude,
      longitude: vehicle.longitude,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01,
    };
    global.display = 1;
    this.mapView.animateToRegion(initialRegion, 500);
  };

  _renderItem = ({ item }) => {
    let nameIcon = '';
    switch (item.dev_status) {
      case '0':
        nameIcon = 'running_car';
        break;
      case '1':
        nameIcon = 'stop_car';
        break;
      case '2':
        nameIcon = 'lost_gps_car';
        break;
      case '3':
        nameIcon = 'lost_gprs_car';
        break;
    }
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={() => {
          this.drawer.closeDrawer();
          const initialRegion = {
            latitude: item.latitude,
            longitude: item.longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          };
          global.display = 1;
          const vehicle = this.props.vehicleTracking.vehicleList.find(
            (obj) => obj.plate === item.plate
          );
          this.setState({
            vehicle,
          });
          this.mapView.animateToRegion(initialRegion, 500);
        }}
      >
        <Image
          style={{
            height: ScaleUtils.floorScale(25),
            width: ScaleUtils.floorScale(25),
          }}
          source={ImageUtil.getImageSource(nameIcon)}
          resizeMode="contain"
        />
        <Text style={styles.listPlate}>{item.vehicle_name}</Text>
      </TouchableOpacity>
    );
  };
  onMarkerPress = (marker) => {
    const vehicle = this.props.vehicleTracking.vehicleList.find(
      (obj) => obj.plate === marker.plate
    );
    // console.log(vehicle);
    this.setState({ vehicle });
    global.display = 1;
    const initialRegion = {
      latitude: vehicle.latitude,
      longitude: vehicle.longitude,
      latitudeDelta: 0.02,
      longitudeDelta: 0.02,
    };

    this.mapView.animateToRegion(initialRegion, 500);
  };
  changeMapTypeCallBack = (mapType) => {
    this.setState({ isShowChangeMapType: false });
    this.props.dispatch({
      type: 'CHANGE_MAP_TYPE',
      data: mapType,
    });
  };
  onClusterPress = (cluster, markers) => {
    let groupedMarkers = markers.map((a) => a.properties.title);
    let groupedDevices = this.props.vehicleTracking.vehicleList.filter((item) =>
      groupedMarkers.includes(item.plate)
    );
    this.setState({ isShowChooseDevice: true, groupedDevices });
  };

  render() {
    const renderVehicle = this.props.vehicleTracking.vehicleList.map(
      (marker) => (
        <Marker.Animated
          // ref={(ref) => {
          //   this.carMoves.set(marker.plate, ref);
          // }}
          onPress={() => {
            this.onMarkerPress(marker);
          }}
          anchor={{ x: 0.5, y: 0.3 }}
          centerOffset={{ x: 0, y: 8.2 }}
          key={marker.plate}
          coordinate={{
            latitude: marker.latitude,
            longitude: marker.longitude,
          }}
          title={marker.plate}
        >
          <VehicleItem
            title={marker.plate}
            dev_status={marker.dev_status}
            direction={marker.direction}
          />
        </Marker.Animated>
      )
    );
    const navigationView = (
      <View
        style={{
          flex: 1,
        }}
      >
        <FlatList
          style={{ width: '100%', marginTop: 10 }}
          data={this.props.vehicleTracking.vehiclesLite}
          renderItem={this._renderItem.bind(this)}
          keyExtractor={(item) => item.plate}
        />
      </View>
    );
    return (
      <View style={styles.container}>
        <DrawerLayout
          drawerBackgroundColor="white"
          drawerWidth={120}
          drawerPosition={'right'}
          drawerLockMode={'locked-closed'}
          ref={(drawer) => {
            return (this.drawer = drawer);
          }}
          keyboardDismissMode="on-drag"
          statusBarBackgroundColor="blue"
          renderNavigationView={() => navigationView}
        >
          <HeaderCommon
            noBack={false}
            showDevice={global.display > 0}
            headerCenter={
              global.display > 0
                ? this.state.vehicle.plate
                : language[this.props.dragonFly.lang].TITLE_VEHICLE_MAP
            }
            goBack={() => this.props.navigation.goBack()}
            headerRight={
              <View
                style={{
                  width: ScaleUtils.floorScale(140),
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}
              >
                {global.display > 0 && global.display < 3 ? (
                  <HeaderStatus
                    dev_status={this.state.vehicle.dev_status}
                    speed={this.state.vehicle.speed}
                    route_time={this.state.vehicle.route_time}
                    v_limit_max={this.state.vehicle.v_limit_max}
                  />
                ) : null}
                {global.display != 2 ? (
                  <TouchableOpacity
                    style={{
                      padding: 8,
                      width: ScaleUtils.floorScale(40),
                      justifyContent: 'center',
                    }}
                    onPress={() => {
                      this.drawer.openDrawer();
                    }}
                  >
                    <Image
                      style={{
                        height: ScaleUtils.floorScale(25),
                        width: ScaleUtils.floorScale(25),
                      }}
                      source={ImageUtil.getImageSource('search_icon')}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={{
                      padding: 8,
                      width: ScaleUtils.floorScale(40),
                      justifyContent: 'center',
                    }}
                    onPress={() => {
                      this.setState({ isShowCalendar: true });
                    }}
                  >
                    <Image
                      style={{
                        height: ScaleUtils.floorScale(25),
                        width: ScaleUtils.floorScale(25),
                      }}
                      source={ImageUtil.getImageSource('calendar')}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                )}
              </View>
            }
          />
          <View style={{ flex: 1, paddingTop: 40 }}>
            <ClusteredMapView
              mapRef={(ref) => (this.mapView = ref)}
              style={styles.map}
              mapType={this.props.vehicleTracking.mapType}
              showsUserLocation={this.state.showsUserLocation}
              showsMyLocationButton={this.state.showsUserLocation}
              loadingEnabled
              zoomEnabled={true}
              initialRegion={this.state.INITIAL_REGION}
              preserveClusterPressBehavior={true}
              onMapReady={() => {
                global.mapReady = true;
                if (global.display !== 3) {
                  this.mapView.animateToRegion(this.state.INITIAL_REGION, 500);
                }
              }}
              onClusterPress={this.onClusterPress}
              renderCluster={(cluster) => {
                const { id, geometry, onPress, properties } = cluster;
                const points = properties.point_count;
                return (
                  <Marker.Animated
                    key={`cluster-${id}`}
                    coordinate={{
                      longitude: geometry.coordinates[0],
                      latitude: geometry.coordinates[1],
                    }}
                    onPress={onPress}
                  >
                    <View
                      style={{
                        padding: 2,
                        marginTop: 30,
                        marginRight: 20,
                        alignItems: 'center',
                      }}
                    >
                      <Image
                        style={{
                          height: ScaleUtils.floorScale(71),
                          width: ScaleUtils.floorScale(56),
                        }}
                        source={ImageUtil.getImageSource('group_car')}
                        resizeMode="contain"
                      />
                      <View
                        style={{
                          position: 'absolute',
                          color: 'white',
                          marginTop: ScaleUtils.floorScale(5),
                          borderRadius: ScaleUtils.floorScale(10),
                          backgroundColor: 'red',
                          paddingHorizontal: ScaleUtils.floorScale(10),
                          //
                          fontWeight: 'bold',
                        }}
                      >
                        <Text style={{ color: 'white' }}>{points}</Text>
                      </View>
                    </View>
                  </Marker.Animated>
                );
              }}
            >
              {global.display === 1 ? (
                <Marker.Animated
                  anchor={{ x: 0.5, y: 0.3 }}
                  centerOffset={{ x: 0, y: 8.2 }}
                  coordinate={{
                    latitude: this.state.vehicle.latitude,
                    longitude: this.state.vehicle.longitude,
                  }}
                  title={this.state.vehicle.plate}
                >
                  <VehicleItem
                    title={this.state.vehicle.vehicle_name}
                    dev_status={this.state.vehicle.dev_status}
                    direction={this.state.vehicle.direction}
                  />
                </Marker.Animated>
              ) : null}
              {global.display === 3 || global.display === 2 ? (
                <Polyline
                  // coordinates={global.coordinates}
                  coordinates={this.state.coordinates}
                  strokeWidth={5}
                  strokeColor={globalSetting.mainColor}
                />
              ) : null}
              {global.display === 2 ? (
                <Marker.Animated
                  ref={(ref) => {
                    this.carReview = ref;
                  }}
                  anchor={{ x: 0.5, y: 0.3 }}
                  centerOffset={{ x: 0, y: 8.2 }}
                  coordinate={{
                    latitude: this.state.vehicle.latitude,
                    longitude: this.state.vehicle.longitude,
                  }}
                  title={this.state.vehicle.plate}
                >
                  <VehicleItem
                    title={this.state.vehicle.vehicle_name}
                    dev_status={this.state.vehicle.dev_status}
                    direction={this.state.vehicle.direction}
                  />
                </Marker.Animated>
              ) : null}
              {global.display === 3 ? (
                <Marker.Animated
                  anchor={{ x: 0.5, y: 0.3 }}
                  centerOffset={{ x: 0, y: 8.2 }}
                  //key={this.state.vehicle.plate}
                  coordinate={{
                    latitude: this.state.vehicle.latitude,
                    longitude: this.state.vehicle.longitude,
                  }}
                  title={this.state.vehicle.plate}
                >
                  <VehicleItem
                    title={this.state.vehicle.vehicle_name}
                    dev_status={this.state.vehicle.dev_status}
                    direction={this.state.vehicle.direction}
                  />
                </Marker.Animated>
              ) : null}
              {global.display === 0 ? renderVehicle : null}
            </ClusteredMapView>
            {global.display === 1 || global.display === 2 ? (
              <VehicleDetail
                vehicle={this.state.vehicle}
                showTrackingTime={this.state.showTrackingTime}
              />
            ) : null}

            {global.display === 1 ? (
              <View
                style={{
                  height: 140,
                  position: 'absolute',
                  bottom: 0,
                  right: 0,
                  width: 100,
                  paddingVertical: 17,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <TouchableOpacity
                  onPress={this.gotoRoutInday}
                  style={{
                    borderColor: 'blue',
                    justifyContent: 'center',
                    borderWidth: 1,
                    borderRadius: 8,
                    height: ScaleUtils.floorScale(45),
                    width: ScaleUtils.floorScale(45),
                  }}
                >
                  <Image
                    style={{
                      height: ScaleUtils.floorScale(40),
                      width: ScaleUtils.floorScale(40),
                    }}
                    source={ImageUtil.getImageSource('review_icon_active')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.gotoDetail}
                  style={{
                    borderColor: 'blue',
                    justifyContent: 'center',
                    borderWidth: 1,
                    borderRadius: 8,
                    height: ScaleUtils.floorScale(45),
                    width: ScaleUtils.floorScale(45),
                  }}
                >
                  <Image
                    style={{
                      height: ScaleUtils.floorScale(40),
                      width: ScaleUtils.floorScale(40),
                    }}
                    source={ImageUtil.getImageSource('detail_icon_active')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            ) : null}
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  isShowChangeMapType: !this.state.isShowChangeMapType,
                })
              }
              style={{
                position: 'absolute',
                bottom: ScaleUtils.floorScale(45),
                marginLeft: ScaleUtils.floorScale(12),
              }}
            >
              <Image
                style={{
                  height: ScaleUtils.floorScale(30),
                  width: ScaleUtils.floorScale(30),
                }}
                source={ImageUtil.getImageSource('map_type')}
                resizeMode="contain"
              />
            </TouchableOpacity>
            {global.display === 2 ? (
              <View
                style={{
                  position: 'absolute',
                  bottom: 10,
                  right: 0,
                  width: '100%',
                  flexDirection: 'row',
                  //justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <TouchableOpacity
                  onPress={this.playReview}
                  style={{
                    marginLeft: ScaleUtils.floorScale(12),
                  }}
                >
                  <Image
                    style={{
                      height: ScaleUtils.floorScale(25),
                      width: ScaleUtils.floorScale(25),
                    }}
                    source={ImageUtil.getImageSource(
                      this.state.playing ? 'stop_icon' : 'play_icon'
                    )}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={
                    () => {
                      let { speed } = this.state;
                      speed *= 2;
                      if (speed > 4) {
                        speed = 1;
                      }
                      this.setState({ speed });
                    }
                    //
                  }
                  style={{ marginLeft: ScaleUtils.floorScale(12) }}
                >
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: globalSetting.mainColor,
                      fontSize: 20,
                    }}
                  >
                    {`${this.state.speed}X`}
                  </Text>
                </TouchableOpacity>
                <Slider
                  style={{
                    flex: 1,
                    height: 30,
                    marginLeft: ScaleUtils.floorScale(12),
                    paddingRight: ScaleUtils.floorScale(12),
                  }}
                  maximumValue={100}
                  minimumValue={0}
                  maximumTrackTintColor={globalSetting.main_text_gray}
                  minimumTrackTintColor={globalSetting.mainColor}
                  step={1}
                  value={this.state.sliderValue}
                  onValueChange={(sliderValue) =>
                    this.setState({ sliderValue })
                  }
                />
              </View>
            ) : null}
            <PopupCalendar
              isShow={this.state.isShowCalendar}
              cancel={() => {
                this.setState({ isShowCalendar: false });
              }}
              callBack={this.onDateChange}
            />
            <PopupChooseDevice
              isShow={this.state.isShowChooseDevice}
              message={language[this.props.dragonFly.lang].GROUP_CHOOSE_VEHICE}
              listData={this.state.groupedDevices}
              onItemPress={this.selectVehicleInGroup}
              onClose={() => {
                this.setState({ isShowChooseDevice: false });
              }}
            />
            <PopupChangeMapType
              isShow={this.state.isShowChangeMapType}
              message={
                language[this.props.dragonFly.lang].CHANGE_MAP_TYPE_TITLE
              }
              callBack={this.changeMapTypeCallBack}
              onClose={() => {
                this.setState({ isShowChangeMapType: false });
              }}
            />
            {global.display === 3 ? (
              <View
                style={{
                  position: 'absolute',
                  top: 10,
                  left: 10,
                  right: 10,
                  alignItems: 'center',
                }}
              >
                <Text
                  style={{}}
                >{`Khoảng cách: ${this.props.vehicleTracking.distanceToVehicle}, thời gian: ${this.props.vehicleTracking.durationToVehicle}`}</Text>
              </View>
            ) : null}
          </View>
          <ShowLoadingView
            ref={(input) => (this.loadingView = input)}
            isModal={true}
            timeout={30000}
          />
        </DrawerLayout>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  listPlate: {
    marginLeft: 5,
  },
  listItem: {
    height: ScaleUtils.floorScale(35),
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: ScaleUtils.floorScale(5),
    borderBottomColor: globalSetting.main_line_color,
    borderBottomWidth: 1,
  },
});

function select(store) {
  return {
    dragonFly: store.dragonFly,
    vehicleTracking: store.vehicleTracking,
  };
}

module.exports = connect(select)(MapsView);
