import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import DeviceInfo from 'react-native-device-info';
import ImageUtil from '../../common/image/ImageUtil';
import language from '../../common/language';
import globalSetting from '../../common/setting';

class MoreView extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <View
          style={{
            backgroundColor: globalSetting.mainColor,
            height: 40,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            paddingTop:
              Platform.OS == 'ios'
                ? Platform.OS == 'ios' && DeviceInfo.hasNotch()
                  ? 40
                  : globalSetting.STATUSBAR_HEIGHT
                : 0,
          }}
        />
        <View style={{
          paddingLeft: 15,
          paddingRight: 15,
        }}>
          <View style={styles.rowSetting}>
            <View style={styles.flexRow}>
              <Image
                style={styles.image}
                source={ImageUtil.getImageSource('use_guide_menu_icon')}
                resizeMode="contain"
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.navigation.navigate('manual');
                }}
              >
                <Text style={styles.settingText}>
                  {language[this.props.dragonFly.lang].MORE_USER_GUID}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.separator} />

          <View style={styles.rowSetting}>
            <View style={styles.flexRow}>
              <Image
                style={styles.image}
                source={ImageUtil.getImageSource('support_menu_icon')}
                resizeMode="contain"
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.navigation.navigate('support');
                }}
              >
                <Text style={styles.settingText}>
                  {language[this.props.dragonFly.lang].MORE_SUPPORT}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.separator} />
          <View style={styles.rowSetting}>
            <View style={styles.flexRow}>
              <Image
                style={styles.image}
                source={ImageUtil.getImageSource('user_info_menu_icon')}
                resizeMode="contain"
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.navigation.navigate('account');
                }}
              >
                <Text style={styles.settingText}>
                  {language[this.props.dragonFly.lang].MORE_ACCOUNT_INFO}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.separator} />
          <View style={styles.rowSetting}>
            <View style={styles.flexRow}>
              <Image
                style={styles.image}
                source={ImageUtil.getImageSource('setting')}
                resizeMode="contain"
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.navigation.navigate('setting');
                }}
              >
                <Text style={styles.settingText}>
                  {language[this.props.dragonFly.lang].MORE_SETTING}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.separator} />
          <View style={styles.rowSetting}>
            <View style={styles.flexRow}>
              <Image
                style={styles.image}
                source={ImageUtil.getImageSource('about_us_menu_icon')}
                resizeMode="contain"
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.navigation.navigate('about');
                }}
              >
                <Text style={styles.settingText}>
                  {language[this.props.dragonFly.lang].MORE_ABOUT}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.separator} />
          <View style={styles.rowSetting}>
            <View style={styles.flexRow}>
              <Image
                style={styles.image}
                source={ImageUtil.getImageSource('logout_menu_icon')}
                resizeMode="contain"
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.dispatch({
                    type: 'LOGOUT',
                  });
                }}
              >
                <Text style={styles.settingText}>
                  {language[this.props.dragonFly.lang].MORE_EXIT}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  separator: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 1,
    backgroundColor: '#CED0CE',
  },
  rowSetting: {
    backgroundColor:globalSetting.main_list_background,
    height: 50,
    alignItems: 'center',
    flexDirection: 'row',
    paddingRight: 20,
  },
  settingText: { fontSize: 18, paddingLeft: 15 },
  flexRow: { flex: 1, flexDirection: 'row' },
  image: { width: 35, height: 35 },
});

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(MoreView);
