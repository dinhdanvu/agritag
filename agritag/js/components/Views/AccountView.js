/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import HeaderCommon from '../Common/HeaderCommon';
import language from '../../common/language';
import { connect } from 'react-redux';

class AccountView extends React.PureComponent {

  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        <HeaderCommon
          noBack={false}
          headerCenter={language[this.props.dragonFly.lang].MORE_ACCOUNT_INFO}
          goBack={() => this.props.navigation.goBack()}
        />
        <Text>{language[this.props.dragonFly.lang].MORE_ACCOUNT_INFO}</Text>
      </View>
    );
  }
}

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(AccountView);
