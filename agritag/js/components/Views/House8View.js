import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

import HeaderCommon from '../Common/HeaderCommon';
import { connect } from 'react-redux';

export default class House8View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentWillUnmount() {
    //this.willFocus.remove();
  }

  componentDidMount() { }

  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        <HeaderCommon
          noBack={false}
          headerCenter={`Ngày ${17}`}
          house_img='house8_color'
          headerRight={'Quay cám lên'}
        />
        <Text>Quay cám lên lơ lửng gần chạm đất</Text>
        
      </View>
    );
  }
}

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(House8View);
