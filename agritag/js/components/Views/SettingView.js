/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import HeaderCommon from '../Common/HeaderCommon';
import language from '../../common/language';
import {connect} from 'react-redux';
class SettingView extends Component {
  static navigationOptions = ({}) => {
    return {
      header: () => <View></View>,
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      lang: this.props.dragonFly.lang,
    };
  }
  setLanguage(lang) {
    if (this.props.dragonFly.lang !== lang) {
      this.setState({lang});
      this.props.dispatch({
        type: 'SET_LANGUAGE',
        data: lang,
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <HeaderCommon
          alignLeft={true}
          goBack={() => this.props.navigation.goBack()}
          navigation={this.props.navigation}
          headerCenter={language[this.props.dragonFly.lang].MAIN_MENU_SETTING}
        />
        <View style={{paddingHorizontal: 20}}>
          <TouchableOpacity
            onPress={() => {
              this.setLanguage(0);
            }}
            style={{
              paddingVertical: 12,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Text style={{fontWeight: 'bold', color: 'black', fontSize: 20}}>
              Tiếng Việt
            </Text>
            {/* <CheckBox
              disable={true}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checked={this.state.lang == 0}
              onPress={() => {
                this.setLanguage(0);
              }}
              checkedColor="#340F84"
              containerStyle={{
                backgroundColor: 'transparent',
                borderRadius: 20,
                borderColor: '#fff',
              }}
            /> */}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.setLanguage(1);
            }}
            style={{
              paddingVertical: 12,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Text style={{fontWeight: 'bold', color: 'black', fontSize: 20}}>
              English
            </Text>
            {/* <CheckBox
              disable={true}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checked={this.state.lang == 1}
              onPress={() => {
                this.setLanguage(1);
              }}
              checkedColor="#340F84"
              containerStyle={{
                backgroundColor: 'transparent',
                borderRadius: 20,
                borderColor: '#fff',
              }}
            /> */}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputForm: {
    height: 35,
    flex: 1,
    paddingBottom: 7,
    color: 'blue',
  },
  listItem: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    color: 'white',
    fontSize: 18,
    marginLeft: 20,
  },
});

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(SettingView);
