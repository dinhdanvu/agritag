import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import StickyHeaderFlatlist from 'react-native-sticky-header-flatlist';
import DeviceItem from '../Common/DeviceItem';
import HeaderCommon from '../Common/HeaderCommon';
import globalSetting from '../../common/setting';
import language from '../../common/language';
import ScaleUtils from '../../common/util/ScaleUtils';
import { connect } from 'react-redux';

class DetailView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vehicle: {},
    };
  }
  componentWillUnmount() {
    if (this.listener) {
      this.listener.remove();
    }
  }

  componentDidMount() {
    this.listener = this.props.navigation.addListener('willFocus', () => {
      // console.log(global.vehicle);
      this.setState({ vehicle: global.vehicle });
    });
  }
  renderSeparator = () => <View style={styles.separator} />;
  render() {
    const DATA = [
      {
        title: `${language[this.props.dragonFly.lang].HEADER_TRACKING_INFO}`,
        itemList: [
          {
            title: `${language[this.props.dragonFly.lang].TRACKING_ADDRESS}`,
            value: this.state.vehicle.address,
          },
          {
            title: `${language[this.props.dragonFly.lang].TRACKING_STATUS}`,
            value: this.state.vehicle.dev_status,
          },
          {
            title: `${
              language[this.props.dragonFly.lang].TRACKING_V_LIMIT_MAX
            }`,
            value: `${this.state.vehicle.v_limit_max} km/h`,
          },
          {
            title: `${language[this.props.dragonFly.lang].TRACKING_ENGINE}`,
            value:
              this.state.vehicle.engine === '1'
                ? language[this.props.dragonFly.lang].ENGINE_STATUS_ON
                : language[this.props.dragonFly.lang].ENGINE_STATUS_OFF,
          },
          {
            title: `${language[this.props.dragonFly.lang].TRACKING_DOOR}`,
            value:
              this.state.vehicle.door_is_open === '1'
                ? language[this.props.dragonFly.lang].DOOR_STATUS_OPEN
                : language[this.props.dragonFly.lang].DOOR_STATUS_OPEN,
          },
          {
            title: `${
              language[this.props.dragonFly.lang].TRACKING_AIR_CONDITIONER
            }`,
            value:
              this.state.vehicle.conditional === '1'
                ? language[this.props.dragonFly.lang].CONDITIONER_STATUS_ON
                : language[this.props.dragonFly.lang].CONDITIONER_STATUS_OFF,
          },
          {
            title: `${
              language[this.props.dragonFly.lang].TRACKING_RUNNING_TIME
            }`,
            value: this.state.vehicle.one_day_running_time,
          },
          {
            title: `${language[this.props.dragonFly.lang].TRACKING_DISTANCE}`,
            value: `${this.state.vehicle.distance} km`,
          },
          {
            title: `${
              language[this.props.dragonFly.lang].TRACKING_NUMBER_OF_STOPS
            }`,
            value: this.state.vehicle.stop_count_on_day,
          },
        ],
      },
      {
        title: `${language[this.props.dragonFly.lang].HEADER_EXTEND}`,
        itemList: [
          {
            title: `${language[this.props.dragonFly.lang].EXTEND_FUEL}`,
            value: this.state.vehicle.stop_count_on_day,
          },
          {
            title: `${language[this.props.dragonFly.lang].EXTEND_TEMPERATURE}`,
            value: this.state.vehicle.stop_count_on_day,
          },
          {
            title: `${language[this.props.dragonFly.lang].EXTEND_HUMIDITY}`,
            value: this.state.vehicle.stop_count_on_day,
          },
          {
            title: `${language[this.props.dragonFly.lang].EXTEND_CONCRETE}`,
            value: this.state.vehicle.stop_count_on_day,
          },
        ],
      },
      {
        title: `${language[this.props.dragonFly.lang].HEADER_VEHICLE_INFO}`,
        itemList: [
          {
            title: `${language[this.props.dragonFly.lang].PLATE_NUMBER}`,
            value: this.state.vehicle.plate,
          },
        ],
      },
      {
        title: `${language[this.props.dragonFly.lang].HEADER_DEVICE}`,
        itemList: [
          {
            title: `${language[this.props.dragonFly.lang].DEVICE_CODE}`,
            value: this.state.vehicle.imei,
          },
        ],
      },
      {
        title: `${language[this.props.dragonFly.lang].HEADER_DRIVER_INFO}`,
        itemList: [
          {
            title: `${language[this.props.dragonFly.lang].DRIVER_FULLNAME}`,
            value: this.state.vehicle.driver_name,
          },
          {
            title: `${language[this.props.dragonFly.lang].DRIVER_LICENSE}`,
            value: this.state.vehicle.driver_code,
          },
          {
            title: `${language[this.props.dragonFly.lang].DRIVER_PHONE_NUMBER}`,
            value: this.state.vehicle.driver_phone,
          },
          {
            title: `${
              language[this.props.dragonFly.lang].DRIVER_NUMBER_OVERSPEED
            }`,
            value: this.state.vehicle.over_speed_count_on_day,
          },
          {
            title: `${language[this.props.dragonFly.lang].DRIVER_DRIVING_TIME}`,
            value: this.state.vehicle.continue_driving_time,
          },
          {
            title: `${language[this.props.dragonFly.lang].DRIVER_EXPIRED}`,
            value: this.state.vehicle.driver_expired,
          },
        ],
      },
    ];
    return (
      <View style={styles.container}>
        <HeaderCommon
          noBack={false}
          headerCenter={language[this.props.dragonFly.lang].TITLE_VEHICLE_INFO}
          goBack={() => this.props.navigation.goBack()}
        />

        <DeviceItem onPress={() => {}} item={this.state.vehicle} />
        <StickyHeaderFlatlist
          keyExtractor={(_, i) => i + ''}
          ItemSeparatorComponent={this.renderSeparator}
          childrenKey={'itemList'}
          renderHeader={({ item }) => {
            return (
              <Text
                style={{
                  paddingVertical: 8,
                  paddingHorizontal: 12,
                  backgroundColor: '#4891CD',
                }}
              >
                {item.title}
              </Text>
            );
          }}
          renderItem={({ item }) => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  backgroundColor: '#D9F0FC',
                  paddingVertical: 5,
                }}
              >
                <View style={{ width: ScaleUtils.floorScale(140) }}>
                  <Text
                    style={{
                      paddingLeft: 12,
                    }}
                  >
                    {`${item.title}:`}
                  </Text>
                </View>
                <View
                  style={{
                    paddingHorizontal: 5,
                    flex: 1,
                  }}
                >
                  <Text>{item.value}</Text>
                </View>
              </View>
            );
          }}
          data={DATA}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  separator: {
    height: 1,
    backgroundColor: globalSetting.main_text_gray,
  },
});

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(DetailView);
