import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

import HeaderCommon from '../Common/HeaderCommon';
import { connect } from 'react-redux';

export default class House1View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      initialRoute: 'home',
    };
  }

  componentWillUnmount() {
    //this.willFocus.remove();
  }

  componentDidMount() { }

  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        <HeaderCommon
          noBack={false}
          headerCenter={`Ngày ${14}`}
          house_img='house1_color'
          headerRight={'Xả chuồng'}
        />
        <Text>Xả chuồng</Text>
        
      </View>
    );
  }
}

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(House1View);
