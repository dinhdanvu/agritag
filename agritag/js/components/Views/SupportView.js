/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import HeaderCommon from '../Common/HeaderCommon';
import language from '../../common/language';
import { connect } from 'react-redux';

class DetailView extends React.PureComponent {

  render() {
    return (
      <View style={styles.container}>
        <HeaderCommon
          noBack={false}
          headerCenter={language[this.props.dragonFly.lang].MORE_SUPPORT}
          goBack={() => this.props.navigation.goBack()}
        />
        <Text>{language[this.props.dragonFly.lang].MORE_SUPPORT}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(DetailView);
