import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  StatusBar
} from 'react-native';

import HeaderCommon from '../Common/HeaderCommon';
import { connect } from 'react-redux';
export default class House10View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentWillUnmount() {
    //this.willFocus.remove();
  }

  componentDidMount() { 
    
  }

  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        <HeaderCommon
          noBack={false}
          headerCenter={`Lịch trình ${14}`}
          house_img='house10_color'
          headerRight={'mỗi ngày phải quay cám, nước, kiểm bin cám'}
        />
        <ScrollView style={styles.scrollView}>
        <Text style={styles.title}>
          Setup chuồng trước ngày gà vô
        </Text>
        <Text style={styles.text}>
          Ống đen để sẵn, quay cám xuống, nước xuống, heat thùng, bật bậu cám, rải tray, chạy cám, xả nước, level nước, đóng toàn bộ cửa sổ phía sau, phía trước để đóng mở tùy mùa(ZicZac), Preheat
        </Text>
        <Text style={styles.title}>
          Trước giờ gà vô
        </Text>
        <Text style={styles.text}>
          Tắt heat dù chờ nguội quay heat lên
        </Text>
        <Text style={styles.title}>
          Ngày đầu tiên khi vô gà
        </Text>
        <Text style={styles.text}>
          Xả nước(mở khóa trước mở van sau), kiểm tra nước nhỏ giọt. Kiểm tra miniVen door, quạt miniVen phải chạy, số SP chạy làm cửa mở. Tất cả relay phải Auto, Breaker tủ điện phải ON, chỉnh banh nước ở Water line vừa phải.
        </Text>
        <Text style={styles.title}>
          Ngày thứ 2
        </Text>
        <Text style={styles.text}>
          Chỉnh banh nước 1 lóng tay, lượm gà handicap, nhớ ghi số lượng gà ở mục Cull mấy ngày ở tuần đầu, level nước, giữ không cho nước nhỏ giọt xuống sân
        </Text>
        <Text style={styles.title}>
          Ngày thứ 3, 4
        </Text>
        <Text style={styles.text}>
          Lượm gà và canh làn cám nếu cần chạy cám ở những tray ngoài, kéo ra để gà làm bằng phẳng chỗ đặt tray rồi kéo vào lại, chú ý khi chạy cám phải tắt đèn
        </Text>
        <Text style={styles.title}>
          Ngày thứ 5
        </Text>
        <Text style={styles.text}>
          Tập gà ngủ 3  hoặc 4 tiếng 1 ngày
        </Text>
        <Text style={styles.title}>
          Ngày thứ 6
        </Text>
        <Text style={styles.text}>
        Diệt gà, ngủ 6 tiếng, bỏ nửa viên .. vào nước
        </Text>
        <Text style={styles.title}>
          Ngày thứ 7
        </Text>
        <Text style={styles.text}>
          Tăng banh nước lên 1 xoắn lớn, level nước, diệt gà
        </Text>
        <Text style={styles.title}>
          Ngày thứ 8
        </Text>
        <Text style={styles.text}>
          Diệt gà, xem thời tiết chuẩn bị ngày đẹp trời để xả chuồng, Quay màn lên trước 1 ngày để hơi nóng ra sau. Cất hết tray cám lên
        </Text>
        <Text style={styles.title}>
          Xả chuồng
        </Text>
        <Text style={styles.text}>
          Tắt Alarm, xả nước, đi level, chạy cám ra sau, đuổi gà, ngăn hàng rào
        </Text>
        <Text style={styles.title}>
          Ngày thứ 9 tới 16
        </Text>
        <Text style={styles.text}>
          Diệt gà
        </Text>
        <Text style={styles.title}>
          Ngày thứ 17
        </Text>
        <Text style={styles.text}>
          Quay cám lên, bổ sung tray nếu có thể
        </Text>
        <Text style={styles.title}>
          Ngày thứ 18 đến 21
        </Text>
        <Text style={styles.text}>
          Ngăn chuồng, phân chia gà cho đều chuồng, tăng banh nước, thêm viên ..
        </Text>
        <Text style={styles.title}>
          Ngày thứ 22 đến 34
        </Text>
        <Text style={styles.text}>
          Diệt gà, kiểm cám
        </Text>
        <Text style={styles.title}>
          Ngày ra gà
        </Text>
        <Text style={styles.text}>
          Cắt cám theo lịch bắt gà, mở cửa, tháo thùng cám, nhá cám dụ gà ăn sạch cám trong tray, chuẩn bị máy cày rửa chuồng
        </Text>
        <Text style={styles.title}>
          Quay cám bắt gà
        </Text>
        <Text style={styles.text}>
          Dim đèn 15%, tắt Alarm, khóa nước, quay (nươc, cám, heat) lên, vặn off bậu nước về gốc 0 vòng rồi vặn trả lại 8 vòng, sau khi bắt xong đi tắt quạt, kiểm tra chuồng
        </Text>
        <Text style={styles.title}>
          Sau khi ra gà
        </Text>
        <Text style={styles.text}>
          Lượm gà, dẹp tray muối, rửa chuồng, rửa quạt, vô dầu mỡ, đánh windroll, chạy quạt chờ chuồng khô đánh level
        </Text>
      </ScrollView>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    marginHorizontal: 20,
  },
  title: {
    fontSize: 42, color:'red'
  },
  text: {
    fontSize: 22,
  },
});

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(House10View);
