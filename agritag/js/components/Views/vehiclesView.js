import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  PermissionsAndroid,
  StatusBar,
  Platform,
} from 'react-native';

import SearchInput, { createFilter } from 'react-native-search-filter';
import DeviceInfo from 'react-native-device-info';
import globalSetting from '../../common/setting';
import DeviceItem from '../Common/DeviceItem';
import SubTotalView from '../Common/SubTotalView';
import language from '../../common/language';
import { connect } from 'react-redux';
import ShowLoadingView from '../Common/ShowLoadingView';
import {
  getVehicleList,
  showToast,
  loadReviewRouteDevices,
} from '../../actions';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';
const myLocation = { latitude: 10.736346, longitude: 106.616706 };
const KEYS_TO_FILTERS = ['plate', 'vehicle_name', 'driver_name'];
var width = Dimensions.get('window').width;

class VehiclesView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vehicle: null,
      searchTerm: '',
      vehicles: [],
      showRunning: true,
      showStop: true,
      showLossGps: true,
      showLossGprs: true,
    };
  }

  componentWillUnmount() {
    if (this.listener) {
      this.listener.remove();
    }
    if (this.getList) {
      clearInterval(this.getList);
    }
  }

  componentDidMount() {
    this.listener = this.props.navigation.addListener('willFocus', () => {
      global.display = 0;
      global.vehicle = {};
      // console.log(this.props.vehicleTracking.vehicle.latitude)
      // if (!this.getList) {
      //   this.getList = setInterval(() => {
      //     if (this.props.dragonFly.isLogin && global.display === 0) {
      //       this.loadingView.showLoadingView();
      //       this.props
      //         .dispatch(getVehicleList())
      //         .then((res) => {
      //           console.log('success');
      //           if (this.loadingView) {
      //             this.loadingView.hideLoadingView();
      //           }
      //         })
      //         .catch((e) => {
      //           if (this.loadingView) {
      //             this.loadingView.hideLoadingView();
      //           }
      //         });
      //     }
      //   }, 10000);
      // }
      this.loadingView.showLoadingView();
      this.props
        .dispatch(getVehicleList())
        .then((res) => {
          console.log('success');
          this.loadingView.hideLoadingView();
        })
        .catch((e) => {
          this.loadingView.hideLoadingView();
        });
    });
  }

  onItemPress(vehicle) {
    global.display = 1;
    global.vehicle = vehicle;
    this.props.navigation.navigate('bando');
  }
  onStatusPress = (status) => {
    switch (status) {
      case 'running':
        this.setState({ showRunning: !this.state.showRunning });
        break;
      case 'stop':
        this.setState({ showStop: !this.state.showStop });
        break;
      case 'loss_gps':
        this.setState({ showLossGps: !this.state.showLossGps });
        break;
      case 'loss_gprs':
        this.setState({ showLossGprs: !this.state.showLossGprs });
        break;
    }
  };
  _renderItem = ({ item, index }) => {
    return (
      <DeviceItem
        onPress={this.onItemPress.bind(this)}
        item={item}
        index={index}
      />
    );
  };
  gotoRoutInday = (vehicle) => {
    this.loadingView.showLoadingView();
    this.props
      .dispatch(loadReviewRouteDevices(vehicle, null))
      .then((res) => {
        console.log('success');
        this.loadingView.hideLoadingView();
        this.props.navigation.navigate('bando');
      })
      .catch((e) => {
        console.log(e);
        this.loadingView.hideLoadingView();
      });
  };
  decode = (t, e) => {
    for (
      var n,
        o,
        u = 0,
        l = 0,
        r = 0,
        d = [],
        h = 0,
        i = 0,
        a = null,
        c = Math.pow(10, e || 5);
      u < t.length;

    ) {
      (a = null), (h = 0), (i = 0);
      do (a = t.charCodeAt(u++) - 63), (i |= (31 & a) << h), (h += 5);
      while (a >= 32);
      (n = 1 & i ? ~(i >> 1) : i >> 1), (h = i = 0);
      do (a = t.charCodeAt(u++) - 63), (i |= (31 & a) << h), (h += 5);
      while (a >= 32);
      (o = 1 & i ? ~(i >> 1) : i >> 1),
        (l += n),
        (r += o),
        d.push([l / c, r / c]);
    }
    return (d = d.map(function (t) {
      return { latitude: t[0], longitude: t[1] };
    }));
  };
  hasLocationPermission = async () => {
    if (
      Platform.OS === 'ios' ||
      (Platform.OS === 'android' && Platform.Version < 23)
    ) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show(
        'Location permission denied by user.',
        ToastAndroid.LONG
      );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show(
        'Location permission revoked by user.',
        ToastAndroid.LONG
      );
    }

    return false;
  };
  gotoFindDevice = async (vehicle) => {
    this.loadingView.showLoadingView();
    global.display = 3;
    // console.log(vehicle.plate)
    const hasLocationPermission = await this.hasLocationPermission();
    if (hasLocationPermission) {
      Geolocation.getCurrentPosition((info) => {
        const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${vehicle.latitude},${vehicle.longitude}&destination=${myLocation.latitude},${myLocation.longitude}&key=AIzaSyCbs33KPFymftJGCSyppT1AjBeiPb_IuUc&mode=driving`;
        fetch(url, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
          },
        })
          .then((response) => response.json())
          .then((responseJson) => {
            const decode = this.decode(
              responseJson.routes[0].overview_polyline.points
            );
            global.coordinates = [
              {
                latitude: vehicle.latitude,
                longitude: vehicle.longitude,
              },
              ...decode,
              {
                latitude: myLocation.latitude,
                longitude: myLocation.longitude,
              },
            ];
            this.props.dispatch({
              type: 'FIND_DEVICE_SUCCESS',
              distanceToVehicle: responseJson.routes[0].legs[0].distance.text,
              durationToVehicle: responseJson.routes[0].legs[0].duration.text,
              points: [
                {
                  latitude: vehicle.latitude,
                  longitude: vehicle.longitude,
                },
                {
                  latitude: myLocation.latitude,
                  longitude: myLocation.longitude,
                },
              ],
            });
            global.display = 3;
            global.vehicle = vehicle;
            this.loadingView.hideLoadingView();
            this.props.navigation.navigate('bando');
          })
          .catch((e) => this.loadingView.hideLoadingView());
      });
    } else {
      console.log('location permission denied');
      this.loadingView.hideLoadingView();
    }
  };
  gotoDetail = (vehicle) => {
    global.display = 0;
    global.vehicle = vehicle;
    this.props.navigation.navigate('detail');
  };
  renderHiddenItem = (data) => (
    <View style={styles.rowBack}>
      <TouchableOpacity
        style={styles.backSideButton}
        onPress={() => this.gotoRoutInday(data.item)}
      >
        <Image
          style={{
            height: ScaleUtils.floorScale(40),
            width: ScaleUtils.floorScale(40),
          }}
          source={ImageUtil.getImageSource('review_icon_active')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.backSideButton}
        onPress={() => this.gotoFindDevice(data.item)}
      >
        <Image
          style={{
            height: ScaleUtils.floorScale(40),
            width: ScaleUtils.floorScale(40),
          }}
          source={ImageUtil.getImageSource('device_icon_active')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.backSideButton}
        onPress={() => this.gotoDetail(data.item)}
      >
        <Image
          style={{
            height: ScaleUtils.floorScale(40),
            width: ScaleUtils.floorScale(40),
          }}
          source={ImageUtil.getImageSource('detail_icon_active')}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
  onRowDidOpen = (rowKey, rowMap) => {
    setTimeout(() => {
      try {
        rowMap[rowKey].closeRow();
      } catch (e) {}
    }, 2000);
  };
  render() {
    
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <View
          style={{
            height: ScaleUtils.floorScale(40),
            backgroundColor: globalSetting.mainColor,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            paddingTop: 0,
          }}
        >
          <Image
            style={{
              position: 'absolute',
              left: 8,
              bottom: 12,
              height: 25,
              width: 25,
            }}
            source={ImageUtil.getImageSource('search_icon')}
            resizeMode="contain"
          />
          <SearchInput
            placeholderTextColor={'#bacce8'}
            style={{
              left: ScaleUtils.floorScale(40),
              top: 2,
              width: ScaleUtils.floorScale(width - 65),
              height: ScaleUtils.floorScale(38),
              color: 'white',
              paddingLeft: 15,
              paddingRight: 25,
              borderRadius: 5,
              opacity: 0.4,
              backgroundColor: '#444444',
            }}
            clearIcon={
              this.state.searchTerm !== '' && (
                <Text
                  style={{ marginTop: -3, fontWeight: 'bold', color: 'white' }}
                >
                  x
                </Text>
              )
            }
            clearIconViewStyles={{
              position: 'absolute',
              top: 15,
              right: -33,
              width: 16,
              height: 16,
              borderRadius: 8,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'red',
            }}
            onChangeText={(term) => this.setState({ searchTerm: term })}
            placeholder={
              language[this.props.dragonFly.lang].SEARCH_VEHICLE_HINT
            }
          />
        </View>
        <View
          style={{
            flex: 1,
          }}
        >
          
        </View>
        <SubTotalView
          running={running}
          showRunning={this.state.showRunning}
          showStop={this.state.showStop}
          showLossGps={this.state.showLossGps}
          showLossGprs={this.state.showLossGprs}
          stop={stop}
          lossGps={lossGps}
          lossGprs={lossGprs}
          sum={sum}
          onPress={this.onStatusPress}
        />
        <ShowLoadingView
          ref={(input) => (this.loadingView = input)}
          isModal={true}
          timeout={30000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  rowBack: {
    alignItems: 'center',
    backgroundColor: globalSetting.gray,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
  },
  backSideButton: { paddingHorizontal: 20 },
});

function select(store) {
  return {
    dragonFly: store.dragonFly,
    vehicleTracking: store.vehicleTracking,
  };
}

module.exports = connect(select)(VehiclesView);
