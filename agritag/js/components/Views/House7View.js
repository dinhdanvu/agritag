import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

import HeaderCommon from '../Common/HeaderCommon';
import { connect } from 'react-redux';

export default class House7View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  componentWillUnmount() {
    //this.willFocus.remove();
  }

  componentDidMount() { }

  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        <HeaderCommon
          noBack={false}
          headerCenter={`Ngày ${22}`}
          house_img='house7_color'
          headerRight={'Dim đèn 20%'}
        />
        <Text>Chỉnh control Dim đèn ON 20%</Text>
        
      </View>
    );
  }
}

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(House7View);
