import React from 'react';
import {View, Text, Image} from 'react-native';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';
export default class VehicleItem extends React.PureComponent {
  render() {
    let nameIcon = '';
    switch (this.props.dev_status) {
      case '0':
        nameIcon = 'xe_dang_chay';
        break;
      case '1':
        nameIcon = 'xe_dung';
        break;
      case '2':
        nameIcon = 'xe_mat_gps';
        break;
      case '3':
        nameIcon = 'xe_mat_tin_hieu';
        break;
    }
    return (
      <View style={{
        alignItems: 'center',width:ScaleUtils.floorScale(60), height:ScaleUtils.floorScale(40)}}>
        <Image
          style={{
            height: ScaleUtils.floorScale(25),
            width: ScaleUtils.floorScale(25),
            transform: [{rotate: `${this.props.direction}deg`}],
          }}
          source={ImageUtil.getImageSource(nameIcon)}
          resizeMode="contain"
        />
        <Text style={{color: 'red', fontSize: 10}}>{this.props.title}</Text>
      </View>
    );
  }
}

