import React from 'react';
import { Text, Image, View } from 'react-native';
import { connect } from 'react-redux';
import language from '../../common/language';
var moment = require('moment');
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';
import globalSetting from '../../common/setting';

class HeaderStatus extends React.PureComponent {
  stopTime(secs) {
    var seconds = parseInt(secs);
    var hour = Math.floor(moment.duration(seconds, 'seconds').asHours());
    var minutes = moment.duration(seconds, 'seconds').minutes();
    if (hour == 0)
      return language[this.props.dragonFly.lang].VEHICLE_STOP + minutes + "'";
    var result =
      language[this.props.dragonFly.lang].VEHICLE_STOP +
      Math.floor(moment.duration(seconds, 'seconds').asHours()) +
      'h' +
      moment.duration(seconds, 'seconds').minutes() +
      "'";
    return (
      language[this.props.dragonFly.lang].VEHICLE_STOP +
      hour +
      'h' +
      minutes +
      "'"
    );
  }
  render() {
    let speedIcon = '';
    let runningStatus = '';
    switch (this.props.dev_status) {
      case '0':
        speedIcon = 'running_icon';
        runningStatus = `${this.props.speed} km/h`;
        break;
      case '1':
        speedIcon = 'thoi_gian';
        runningStatus = this.stopTime(this.props.route_time);
        break;
      case '2':
        speedIcon = 'thoi_gian';
        runningStatus = this.stopTime(this.props.route_time);
        break;
      case '3':
        nameIcon = 'lost_gprs_car';
        runningStatus = this.stopTime(this.props.route_time);
        break;
    }
    return (
      <View
        style={{
          height: ScaleUtils.floorScale(30),
          backgroundColor: 'white',
          borderRadius: 5,
          paddingHorizontal: 5,
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Image
            style={{
              height: ScaleUtils.floorScale(20),
              width: ScaleUtils.floorScale(20),
            }}
            source={ImageUtil.getImageSource(speedIcon)}
            resizeMode="contain"
          />
          <Text
            numberOfLines={1}
            ellipsizeMode="head"
            style={{
              fontSize: 12,
              color: globalSetting.mainColor,
              marginLeft: 4,
            }}
          >
            {runningStatus}
          </Text>
          {this.props.dev_status === '0' ? (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 2,
                borderColor: 'red',
                borderRadius: 10,
                marginLeft: 4,
                width: ScaleUtils.floorScale(20),
                height: ScaleUtils.floorScale(20),
              }}
            >
              <Text
                numberOfLines={1}
                ellipsizeMode="head"
                style={{ fontSize: 10 }}
              >
                {this.props.v_limit_max}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
    );
  }
}

function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(HeaderStatus);
