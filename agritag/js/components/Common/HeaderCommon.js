import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  Image,
  StatusBar,
} from 'react-native';
import { connect } from 'react-redux';
import DeviceInfo from 'react-native-device-info';
import globalSetting from '../../common/setting';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';

class HeaderCommon extends React.PureComponent {
  render() {
    let alignLeft = {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'row',
    };
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          height: ScaleUtils.floorScale(40),
          backgroundColor: globalSetting.mainColor,
          shadowColor: 'black',
          shadowOpacity: 0.1,
          shadowRadius: 3,
          shadowOffset: {
            height: 3,
          },
          elevation: 4,
          paddingTop:
            0,
        }}
      >
        <StatusBar barStyle="light-content" />
        {this.props.noBack ? null : (
          <TouchableOpacity
            onPress={() => {
              this.props.goBack();
            }}
          >
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 10,
              }}
            >
              <Image
                style={{
                  height: ScaleUtils.floorScale(25),
                  width: ScaleUtils.floorScale(25),
                }}
                source={ImageUtil.getImageSource('prev_white_icon')}
                resizeMode="contain"
              />
            </View>
          </TouchableOpacity>
        )}
        <View style={alignLeft}>
          <Image
            style={{
              height: ScaleUtils.floorScale(18),
              width: ScaleUtils.floorScale(18),
            }}
            source={ImageUtil.getImageSource(this.props.house_img)}
            resizeMode="contain"
          />
          <Text
            style={
              {
                marginLeft: 10,
                fontWeight: 'bold',
                fontSize: ScaleUtils.floorScale(15),
                color: globalSetting.main_text_color,
              }
            }
          >
            {this.props.headerCenter}
          </Text>
        </View>
        {this.props.headerRight ? (
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={
              {
                marginRight: 10,
                fontWeight: 'bold',
                fontSize: ScaleUtils.floorScale(15),
                color: globalSetting.main_text_color,
              }
            }>{this.props.headerRight}</Text>
          </View>
        ) : null}
      </View>
    );
  }
}
function select(store) {
  return {};
}

module.exports = connect(select)(HeaderCommon);
