import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  NativeModules,
  FlatList,
} from 'react-native';
import globalSetting from '../../common/setting';
import ImageUtil from '../../common/image/ImageUtil';
import ScaleUtils from '../../common/util/ScaleUtils';
const {PlatformConstants} = NativeModules;
const deviceType = PlatformConstants.interfaceIdiom;

export default class PopupChooseDevice extends React.PureComponent {
  _renderItem({item, index}) {
    const temp = {
      fontSize: 14,
    }
    let nameIcon = 'arrow_gray';
    let color = 'black';
    switch (item[3]) {
      case '0':
        nameIcon = 'arrow_blue';
        color = globalSetting.main_text_blue;
        break;
        nameIcon = 'arrow_red';
        color = globalSetting.main_text_red;
        break;
      case '2':
        nameIcon = 'arrow_yellow';
        color = globalSetting.main_text_yellow;
        break;
      case '3':
        nameIcon = 'arrow_gray';
        color = globalSetting.main_text_gray;
        break;
    }
    const textColor = { color }
    return (
      <TouchableOpacity onPress={() => {this.props.onItemPress(item.plate)}}>
        <View
          style={{
            justifyContent: 'center', alignItems: 'center',
            flexDirection: 'row',
            borderBottomColor: 'black',
            borderBottomWidth: 1,
            paddingHorizontal: 10,
            paddingVertical:5,
          }}
        >
          <Image
            style={{
              height: ScaleUtils.floorScale(25),
              width: ScaleUtils.floorScale(25),
              
            }}
            source={ImageUtil.getImageSource(nameIcon)}
            resizeMode="contain"
          />
          <View
            style={{
              marginLeft: 10,
              flex: 1,
            }}
          >
            <Text style={[temp, textColor]}>{item.vehicle_name}</Text>
          </View>
          <View
            style={{
              marginLeft: 10,
              justifyContent: 'center',
            }}
          >
            <Text style={[temp, textColor]}>{item.driver_name}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    if (!this.props.isShow) {
      return null;
    }
    return (
      <View style={styles.container}>
        <View style={deviceType === 'pad' ? styles.modalIpad : styles.modal}>
          <View style={styles.title}>
            <Text
              style={
                deviceType === 'pad'
                  ? styles.textmessageIpad
                  : styles.textmessage
              }
            >
              {this.props.message}
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.onClose();
              }}
            >
              <Image
                style={{
                  height: ScaleUtils.floorScale(25),
                  width: ScaleUtils.floorScale(25),
                }}
                source={ImageUtil.getImageSource('close')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.listDevice}>
            <FlatList
              contentContainerStyle={{justifyContent: 'center'}}
              keyExtractor={(item, index) => index.toString()}
              data={this.props.listData}
              renderItem={this._renderItem.bind(this)}
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: ScaleUtils.floorScale(100),
    bottom: ScaleUtils.floorScale(100),
    left: ScaleUtils.floorScale(20),
    right: ScaleUtils.floorScale(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    backgroundColor: globalSetting.mainColor,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    height: ScaleUtils.floorScale(34),
    paddingRight: 5,
    marginTop: 5,
    marginHorizontal: 3,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  listDevice: {
    paddingTop: 5,
    paddingBottom: 10,
    height: ScaleUtils.floorScale(225),
  },
  modalIpad: {
    width: ScaleUtils.floorScale(200),
    backgroundColor: 'white',
    justifyContent: 'space-around',
    borderRadius: 13,
  },
  modal: {
    width: ScaleUtils.floorScale(300),
    backgroundColor: 'white',
    justifyContent: 'space-around',
    borderRadius: 13,
  },
  textmessage: {
    flex: 1,
    color: globalSetting.main_text_color,
    fontWeight: 'bold',
    fontSize: ScaleUtils.floorScale(14),
    textAlign: 'left',
    paddingLeft: 10,
  },
  textmessageIpad: {
    flex: 1,
    color: globalSetting.main_text_color,
    fontWeight: 'bold',
    fontSize: ScaleUtils.floorScale(9),
    textAlign: 'left',
    paddingLeft: 10,
  },
});
