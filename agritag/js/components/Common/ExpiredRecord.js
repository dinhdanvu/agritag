import React, {Component} from 'react';
import {Text,View} from 'react-native';
import globalSetting from '../../common/setting';

export default class ExpiredRecord extends Component {
  render() {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            flex: 1,
          }}
        >
          <View
            style={{
              flex: 1,
            }}
          >
            <Text style={{fontSize: 11, color: 'red'}}>
              {this.props.dayExpired}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            flex: 1,
          }}
        >
          <View
            style={{
              flex: 1,
            }}
          >
            <Text
              numberOfLines={1}
              ellipsizeMode="head"
              style={{fontSize: 11, color:globalSetting.main_text_gray}}
            >
              {this.props.contact}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
