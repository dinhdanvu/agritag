import React from 'react';
import {Text, TouchableOpacity, Image, View} from 'react-native';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';

export default class DeviceStatus extends React.PureComponent {
  render() {
    let nameIcon = '';
    switch (this.props.status) {
      case 'running':
        nameIcon = !this.props.active
          ? 'device_status_running_unactive'
          : 'device_status_running_active';
        break;
      case 'loss_gps':
        nameIcon = !this.props.active
          ? 'device_status_loss_gps_unactive'
          : 'device_status_loss_gps_active';
        break;
      case 'stop':
        nameIcon = !this.props.active
          ? 'device_status_stop_unactive'
          : 'device_status_stop_active';
        break;
      case 'loss_gprs':
        nameIcon = !this.props.active
          ? 'device_status_loss_gprs_unactive'
          : 'device_status_loss_gprs_active';
        break;
      case 'sum':
        nameIcon = 'sum_icon';
        break;
    }

    return (
      <TouchableOpacity style={{alignItems: 'center'}}
        onPress={() => {
          this.props.onPress(this.props.status);
        }}
      >
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{
              height: ScaleUtils.floorScale(22),
              width: ScaleUtils.floorScale(22),
              marginRight: 4,
            }}
            source={ImageUtil.getImageSource(nameIcon)}
            resizeMode="contain"
          />
          <Text>{this.props.quantity}</Text>
        </View>
        <Text style={{fontSize:12}}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}
