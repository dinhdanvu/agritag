/* eslint-disable consistent-this */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, ActivityIndicator, StyleSheet, Image} from 'react-native';
import ImageUtil from '../../common/image/ImageUtil';
export default class showLoadingView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: this.props.isShow ? this.props.isShow : false,
    };
    this.id = new Date().getTime().toString();
    this.notchangeID = new Date().getTime().toString();
    this.timeout = this.props.timeout ? this.props.timeout : 2000;
    this.hideLoadingView = this.hideLoadingView.bind(this);
    this.showLoadingViewWithTimeOut = this.showLoadingViewWithTimeOut.bind(
      this
    );
    this.showLoadingView = this.showLoadingView.bind(this);
    this.mounted = null;
  }

  async showLoadingViewWithTimeOut(timeout) {
    try {
      this.setState({
        isShow: true,
      });
    } catch (error) {}

    var self = this;
    this.id = new Date().getTime().toString();
    var id = this.id;
    setTimeout(function () {
      if (id === self.id) {
        self.hideLoadingView();
      }
    }, timeout);
  }

  async showLoadingView() {
    try {
      this.setState({
        isShow: true,
      });
    } catch (error) {}

    var self = this;
    this.id = new Date().getTime().toString();
    var id = this.id;
    setTimeout(function () {
      if (id === self.id) {
        self.hideLoadingView();
      }
    }, this.timeout);
  }

  hideLoadingView() {
    if (this.state.isShow !== false && this.mounted) {
      try {
        this.setState({
          isShow: false,
        });
      } catch (error) {}
    }
  }

  componentDidMount() {
    this.mounted = true;
    if (this.state.isShow) {
      var self = this;
      this.id = new Date().getTime().toString();
      var id = this.id;
      setTimeout(function () {
        if (id === self.id) {
          try {
            self.setState({isShow: false});
          } catch (error) {}
        }
      }, this.timeout);
    }
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  render() {
    if (this.state.isShow === false) {
      return null;
    }
    return (
      <View style={styles.container}>
        <View style={{padding: 5}}>
          <Image
            style={{
              position: 'absolute',
              left: 10,
              top: 10,
              height: 40,
              width: 40,
            }}
            source={ImageUtil.getImageSource('logo_small')}
            resizeMode="contain"
          />
          <ActivityIndicator
            animating={this.state.isShow}
            size={50}
            color="#0000ff"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    //backgroundColor: 'rgba(255,255,255,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
