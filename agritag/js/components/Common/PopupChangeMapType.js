import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  NativeModules,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import language from '../../common/language';
import globalSetting from '../../common/setting';
import ImageUtil from '../../common/image/ImageUtil';
import ScaleUtils from '../../common/util/ScaleUtils';
const {PlatformConstants} = NativeModules;
const deviceType = PlatformConstants.interfaceIdiom;

class PopupChangeMapType extends React.PureComponent {
  render() {
    if (!this.props.isShow) {
      return null;
    }
    return (
      <View style={styles.container}>
        <View style={deviceType === 'pad' ? styles.modalIpad : styles.modal}>
          <View style={styles.title}>
            <Text
              style={
                deviceType === 'pad'
                  ? styles.textmessageIpad
                  : styles.textmessage
              }
            >
              {this.props.message}
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.onClose();
              }}
            >
              <Image
                style={{
                  height: ScaleUtils.floorScale(25),
                  width: ScaleUtils.floorScale(25),
                }}
                source={ImageUtil.getImageSource('close')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <TouchableOpacity onPress={() => this.props.callBack('standard')}>
              <View
                style={{
                  height: ScaleUtils.floorScale(60),
                    width: ScaleUtils.floorScale(60),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image
                  style={{
                    height: ScaleUtils.floorScale(40),
                    width: ScaleUtils.floorScale(40),
                  }}
                  source={ImageUtil.getImageSource('map_type_standard')}
                  resizeMode="contain"
                />
                <Text>
                    {language[this.props.dragonFly.lang].MAP_TYPE_STANDARD}
                  </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.callBack('satellite')}>
              <View
                style={{
                  height: ScaleUtils.floorScale(60),
                    width: ScaleUtils.floorScale(60),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image
                  style={{
                    height: ScaleUtils.floorScale(40),
                    width: ScaleUtils.floorScale(40),
                  }}
                  source={ImageUtil.getImageSource('map_type_satellite')}
                  resizeMode="contain"
                />
                <Text>
                    {language[this.props.dragonFly.lang].MAP_TYPE_SATELLITE}
                  </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.callBack('hybrid')}>
              <View
                style={{
                  height: ScaleUtils.floorScale(60),
                    width: ScaleUtils.floorScale(60),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image
                  style={{
                    height: ScaleUtils.floorScale(40),
                    width: ScaleUtils.floorScale(40),
                  }}
                  source={ImageUtil.getImageSource('map_type_hybrid')}
                  resizeMode="contain"
                />
                <Text>
                    {language[this.props.dragonFly.lang].MAP_TYPE_HYBRID}
                  </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(PopupChangeMapType);
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: ScaleUtils.floorScale(100),
    bottom: ScaleUtils.floorScale(150),
    left: ScaleUtils.floorScale(20),
    right: ScaleUtils.floorScale(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    backgroundColor: globalSetting.mainColor,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    height: ScaleUtils.floorScale(34),
    paddingRight: 5,
    marginTop: 5,
    marginHorizontal: 3,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  item: {
    paddingTop: 5,
    paddingBottom: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    height: ScaleUtils.floorScale(125),
  },
  modalIpad: {
    width: ScaleUtils.floorScale(200),
    backgroundColor: 'white',
    justifyContent: 'space-around',
    borderRadius: 13,
  },
  modal: {
    width: ScaleUtils.floorScale(300),
    backgroundColor: 'white',
    justifyContent: 'space-around',
    borderRadius: 13,
  },
  textmessage: {
    flex: 1,
    color: globalSetting.main_text_color,
    fontWeight: 'bold',
    fontSize: ScaleUtils.floorScale(14),
    textAlign: 'left',
    paddingLeft: 10,
  },
  textmessageIpad: {
    flex: 1,
    color: globalSetting.main_text_color,
    fontWeight: 'bold',
    fontSize: ScaleUtils.floorScale(9),
    textAlign: 'left',
    paddingLeft: 10,
  },
});
