import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
var moment = require('moment');
import InfoRecord from './InfoRecord';
import language from '../../common/language';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';
import globalSetting from '../../common/setting';

class DeviceItem extends React.PureComponent {
  stopTime(secs) {
    var seconds = parseInt(secs);
    var hour = Math.floor(moment.duration(seconds, 'seconds').asHours());
    var minutes = moment.duration(seconds, 'seconds').minutes();
    if (hour == 0)
      return language[this.props.dragonFly.lang].VEHICLE_STOP + minutes + "'";
    var result =
      language[this.props.dragonFly.lang].VEHICLE_STOP +
      Math.floor(moment.duration(seconds, 'seconds').asHours()) +
      'h' +
      moment.duration(seconds, 'seconds').minutes() +
      "'";
    return (
      language[this.props.dragonFly.lang].VEHICLE_STOP +
      hour +
      'h' +
      minutes +
      "'"
    );
  }

  render() {
    let nameIcon = '';
    let speedIcon = '';
    let runningStatus = '';
    let color = 'black';
    if (this.props.item.isExpiredService > 1) {
      color = globalSetting.main_text_gray;
      nameIcon = 'xe_het_han';
    } else
      switch (this.props.item.dev_status) {
        case '0':
          nameIcon = 'running_car';
          speedIcon = 'running_icon';
          color = globalSetting.main_text_blue;
          runningStatus = `${this.props.item.speed} km/h`;
          break;
        case '1':
          nameIcon = 'stop_car';
          speedIcon = 'thoi_gian';
          color = globalSetting.main_text_red;
          runningStatus = this.stopTime(this.props.item.route_time);
          break;
        case '2':
          nameIcon = 'lost_gps_car';
          speedIcon = 'thoi_gian';
          color = globalSetting.main_text_yellow;
          runningStatus = this.stopTime(this.props.item.route_time);
          break;
        case '3':
          nameIcon = 'lost_gprs_car';
          speedIcon = 'thoi_gian';
          color = globalSetting.main_text_gray;
          runningStatus = this.stopTime(this.props.item.route_time);
          break;
      }
    const textColor = { color };
    return (
      <TouchableHighlight
        onPress={() => {
          this.props.onPress(this.props.item);
        }}
      >
        <View
          style={{
            backgroundColor: globalSetting.main_list_background,
            borderBottomColor: globalSetting.main_line_color,
            borderBottomWidth: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            paddingVertical: 3,
          }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: ScaleUtils.floorScale(55),
            }}
          >
            <Image
              style={{
                height: ScaleUtils.floorScale(38),
                width: ScaleUtils.floorScale(38),
              }}
              source={ImageUtil.getImageSource(nameIcon)}
              resizeMode="contain"
            />
            {this.props.item.engine === '1' ? (
              <Image
                style={{
                  position: 'absolute',
                  right: 1,
                  top: ScaleUtils.floorScale(16),
                  height: ScaleUtils.floorScale(14),
                  width: ScaleUtils.floorScale(14),
                }}
                source={ImageUtil.getImageSource('engine')}
                resizeMode="contain"
              />
            ) : null}
          </View>
          <View
            style={{
              flex: 1,
              marginLeft: 2,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
              }}
            >
              <View
                style={{
                  width: ScaleUtils.floorScale(120),
                }}
              >
                <Text style={[{ fontSize: 11 }, textColor]}>
                  {this.props.item.plate}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                }}
              >
                <Text
                  numberOfLines={1}
                  ellipsizeMode="head"
                  style={[{ fontSize: 12 }, textColor]}
                >
                  {this.props.item.driver_name}
                </Text>
              </View>
              <Text
                style={[
                  {
                    fontSize: 10,
                    width: ScaleUtils.floorScale(35),
                    textAlign: 'center',
                    justifyContent: 'center',
                  },
                  textColor,
                ]}
              >
                {this.props.item.continue_driving_time}
              </Text>
            </View>
            <InfoRecord
              isExpiredService={this.props.item.isExpiredService}
              numberExpired={this.props.item.number_expired}
              textColor={textColor}
              speedIcon={speedIcon}
              runningStatus={runningStatus}
              address={this.props.item.address}
              distance={this.props.item.distance_on_day}
            />
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}
const styles = StyleSheet.create({
  container: {
    color: globalSetting.main_text_color,
  },
});
module.exports = connect(select)(DeviceItem);
