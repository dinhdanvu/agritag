import React from 'react';
import {View, Text, Image} from 'react-native';
import {connect} from 'react-redux';
import language from '../../common/language';
import globalSetting from '../../common/setting';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';
import DeviceStatus from './DeviceStatus';

class SubTotalView extends React.PureComponent {
  render() {
    return (
      <View
        style={{
          width: globalSetting.sWidth,
          height: 48,
          backgroundColor: globalSetting.main_gray_background,
          justifyContent: 'center',
          alignItems: 'center',
          borderTopWidth: 1,
          flexDirection: 'row',
          justifyContent: 'space-around',
        }}
      >
        <DeviceStatus
          status="running"
          quantity={this.props.running}
          active={this.props.showRunning}
          onPress={this.props.onPress}
          title={language[this.props.dragonFly.lang].STATUS_RUNNING}
        />
        <DeviceStatus
          status="stop"
          quantity={this.props.stop}
          active={this.props.showStop}
          onPress={this.props.onPress}
          title={language[this.props.dragonFly.lang].STATUS_STOP}
        />
        <DeviceStatus
          status="loss_gps"
          quantity={this.props.lossGps}
          active={this.props.showLossGps}
          onPress={this.props.onPress}
          title={language[this.props.dragonFly.lang].STATUS_LOSSGPS}
        />
        <DeviceStatus
          status="loss_gprs"
          quantity={this.props.lossGprs}
          active={this.props.showLossGprs}
          onPress={this.props.onPress}
          title={language[this.props.dragonFly.lang].STATUS_LOSSGPRS}
        />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{
              height: ScaleUtils.floorScale(22),
              width: ScaleUtils.floorScale(22),
              marginRight: 4,
            }}
            source={ImageUtil.getImageSource('sum_icon')}
            resizeMode="contain"
          />
          <Text style={{width:25}}>{this.props.sum}</Text>
        </View>
      </View>
    );
  }
}
function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(SubTotalView);