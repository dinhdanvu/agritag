import React from 'react';
import { Text, StyleSheet, Image, View } from 'react-native';
import { connect } from 'react-redux';
import language from '../../common/language';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';
import globalSetting from '../../common/setting';

class VehicleDetail extends React.PureComponent {
  render() {
    let batteryLevel = this.props.vehicle.battery_level;
    batteryLevel = Math.round(batteryLevel / 10);
    let batteryIcon = `pin_${batteryLevel}`;
    let onOffIcon = '';
    let engineStatusText = '';
    let doorIcon = '';
    let doorStatusText = '';
    let conditionerIcon = '';
    let conditionerStatusText = '';
    if (this.props.vehicle.engine === '1') {
      onOffIcon = 'dong_co_mo';
      engineStatusText = language[this.props.dragonFly.lang].ENGINE_STATUS_ON;
    } else {
      onOffIcon = 'dong_co_tat';
      engineStatusText = language[this.props.dragonFly.lang].ENGINE_STATUS_OFF;
    }
    if (this.props.vehicle.door_is_open === '1') {
      doorIcon = 'xe_mo_cua';
      doorStatusText = language[this.props.dragonFly.lang].DOOR_STATUS_OPEN;
    } else {
      doorIcon = 'xe_dong_cua';
      doorStatusText = language[this.props.dragonFly.lang].DOOR_STATUS_CLOSE;
    }
    if (this.props.vehicle.conditional === '1') {
      conditionerIcon = 'may_lanh_mo';
      conditionerStatusText =
        language[this.props.dragonFly.lang].CONDITIONER_STATUS_ON;
    } else {
      conditionerIcon = 'may_lanh_tat';
      conditionerStatusText =
        language[this.props.dragonFly.lang].CONDITIONER_STATUS_OFF;
    }

    return (
      <View
        style={{
          height: 130,
          backgroundColor: globalSetting.main_detail_background,
          position: 'absolute',
          top: 0,
          left: 0,
          width: globalSetting.sWidth,
        }}
      >
        <View
          style={{
            marginHorizontal: 10,
            height: ScaleUtils.floorScale(20),
            justifyContent: 'flex-end',
            flexDirection: 'row',
          }}
        >
          <View style={{ flexDirection: 'row', flex: 1 }}>
            <Image
              style={{
                height: ScaleUtils.floorScale(20),
                width: ScaleUtils.floorScale(20),
              }}
              source={ImageUtil.getImageSource('user')}
              resizeMode="contain"
            />
            <Text style={{ color: 'white' }}>
              {this.props.vehicle.driver_name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: ScaleUtils.floorScale(50),
            }}
          >
            <Image
              style={{
                height: ScaleUtils.floorScale(20),
                width: ScaleUtils.floorScale(20),
              }}
              source={ImageUtil.getImageSource('co_gps')}
              resizeMode="contain"
            />
            <Text style={{ fontSize: 10, color: 'white' }}>
              {this.props.vehicle.satellites}
            </Text>
            <Image
              style={{
                marginLeft: 5,
                height: ScaleUtils.floorScale(20),
                width: ScaleUtils.floorScale(20),
              }}
              source={ImageUtil.getImageSource(batteryIcon)}
              resizeMode="contain"
            />
          </View>
        </View>
        {this.props.showTrackingTime?(
          <View
          style={{
            marginLeft: 10,
          }}
        >
          <Text style={{ color: 'white', fontSize: 11 }}>{`${
            language[this.props.dragonFly.lang].TIMESTAMP
          }: ${this.props.vehicle.trk_time}`}</Text>
        </View>
        ):null}
        <View
          style={{
            marginTop: 5,
            height: ScaleUtils.floorScale(35),
            justifyContent: 'space-around',
            flexDirection: 'row',
          }}
        >
          <View
            style={{
              width: ScaleUtils.floorScale(50),
              alignItems: 'center',
            }}
          >
            <Image
              style={{
                height: ScaleUtils.floorScale(15),
                width: ScaleUtils.floorScale(15),
              }}
              source={ImageUtil.getImageSource(onOffIcon)}
              resizeMode="contain"
            />
            <Text style={{ color: 'white', fontSize: 10 }}>
              {engineStatusText}
            </Text>
          </View>
          <View
            style={{ width: ScaleUtils.floorScale(50), alignItems: 'center' }}
          >
            <Image
              style={{
                height: ScaleUtils.floorScale(20),
                width: ScaleUtils.floorScale(20),
              }}
              source={ImageUtil.getImageSource(doorIcon)}
              resizeMode="contain"
            />
            <Text style={{ color: 'white', fontSize: 10 }}>
              {doorStatusText}
            </Text>
          </View>
          <View
            style={{ width: ScaleUtils.floorScale(50), alignItems: 'center' }}
          >
            <Image
              style={{
                height: ScaleUtils.floorScale(20),
                width: ScaleUtils.floorScale(20),
              }}
              source={ImageUtil.getImageSource(conditionerIcon)}
              resizeMode="contain"
            />
            <Text style={{ color: 'white', fontSize: 10 }}>
              {conditionerStatusText}
            </Text>
          </View>
          <View
            style={{ width: ScaleUtils.floorScale(70), alignItems: 'center' }}
          >
            <View style={{ flexDirection: 'row' }}>
              <Image
                style={{
                  height: ScaleUtils.floorScale(20),
                  width: ScaleUtils.floorScale(20),
                }}
                source={ImageUtil.getImageSource('quang_duong')}
                resizeMode="contain"
              />
              <Text style={{ color: 'white', fontSize: 10 }}>
                {parseFloat(this.props.vehicle.distance_on_day).toFixed(2)}
                {' km'}
              </Text>
            </View>
            <Text style={{ color: 'white', fontSize: 10 }}>
              {language[this.props.dragonFly.lang].DISTANCE}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            marginLeft: 10,
            height: ScaleUtils.floorScale(20),
          }}
        >
          <Image
            style={{
              height: ScaleUtils.floorScale(20),
              width: ScaleUtils.floorScale(20),
            }}
            source={ImageUtil.getImageSource('map_icon_white')}
            resizeMode="contain"
          />
          <Text
            numberOfLines={1}
            ellipsizeMode="head"
            style={{ color: 'white', fontSize: 10 }}
          >
            {this.props.vehicle.address}
          </Text>
        </View>
      </View>
    );
  }
}
function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(VehicleDetail);
