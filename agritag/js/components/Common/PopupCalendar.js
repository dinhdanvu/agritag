import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  NativeModules,
} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';
import { connect } from 'react-redux';
import language from '../../common/language';
import globalSetting from '../../common/setting';
import ScaleUtils from '../../common/util/ScaleUtils';
const { PlatformConstants } = NativeModules;
const deviceType = PlatformConstants.interfaceIdiom;

class PopupCalendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    };
  }
  render() {
    if (!this.props.isShow) {
      return null;
    }
    return (
      <View style={styles.container}>
        <View style={deviceType === 'pad' ? styles.modalIpad : styles.modal}>
          <CalendarPicker
            todayBackgroundColor="red"
            selectedDayTextColor="white"
            width={globalSetting.sWidth - ScaleUtils.floorScale(50)}
            onDateChange={(date) => this.setState({ date })}
          />
          <View
            style={{
              flexDirection: 'row',
              marginTop: 5,
              height: ScaleUtils.floorScale(40),
            }}
          >
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.props.cancel();
              }}
            >
              <Text style={{}}>
                {language[this.props.dragonFly.lang].MORE_EXIT}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.props.callBack(this.state.date);
              }}
            >
              <Text style={{}}>
                {language[this.props.dragonFly.lang].MORE_OK}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: ScaleUtils.floorScale(70),
    bottom: ScaleUtils.floorScale(80),
    left: ScaleUtils.floorScale(20),
    right: ScaleUtils.floorScale(20),
    alignItems: 'center',
  },

  modalIpad: {
    width: ScaleUtils.floorScale(200),
    backgroundColor: 'white',
    justifyContent: 'space-around',
  },
  modal: {
    width: ScaleUtils.floorScale(300),
    backgroundColor: 'white',
    justifyContent: 'space-around',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: globalSetting.main_text_gray,
  },
});
function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(PopupCalendar);
