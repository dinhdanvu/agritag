import React from 'react';
import { Text, Image, View } from 'react-native';
import { connect } from 'react-redux';
import language from '../../common/language';
import ScaleUtils from '../../common/util/ScaleUtils';
import ImageUtil from '../../common/image/ImageUtil';
import globalSetting from '../../common/setting';

class InfoRecord extends React.PureComponent {
  render() {
    if (this.props.isExpiredService >= 2) {
      return (
        <View>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
            }}
          >
            <View
              style={{
                flex: 1,
              }}
            >
              <Text style={{ fontSize: 11, color: 'red' }}>
                Hết hạn dịch vụ
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              flex: 1,
            }}
          >
            <View
              style={{
                flex: 1,
              }}
            >
              <Text
                numberOfLines={1}
                ellipsizeMode="head"
                style={{ fontSize: 11, color: globalSetting.main_text_gray }}
              >
                {language[this.props.dragonFly.lang].SERVICE_EXPIRED_CONTACT}
              </Text>
            </View>
          </View>
        </View>
      );
    } else if (this.props.isExpiredService === 1) {
      return (
        <View>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              flex: 1,
            }}
          >
            <View
              style={{
                flex: 1,
              }}
            >
              <Text style={{ fontSize: 11, color: 'red' }}>
                {`${language[this.props.dragonFly.lang].SERVICE_EXPIRED} ${
                  this.props.numberExpired
                } ${language[this.props.dragonFly.lang].SERVICE_EXPIRED_LEFT}`}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
            }}
          >
            <View
              style={{
                flex: 1,
              }}
            >
              <Text
                numberOfLines={1}
                ellipsizeMode="head"
                style={{ fontSize: 11, color: globalSetting.main_text_gray }}
              >
                {language[this.props.dragonFly.lang].SERVICE_EXPIRED_CONTACT}
              </Text>
            </View>
          </View>
        </View>
      );
    } else {
      return (
        <View>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                width: ScaleUtils.floorScale(120),
              }}
            >
              <Image
                style={{
                  height: ScaleUtils.floorScale(11),
                  width: ScaleUtils.floorScale(11),
                }}
                source={ImageUtil.getImageSource(this.props.speedIcon)}
                resizeMode="contain"
              />
              <Text
                numberOfLines={1}
                ellipsizeMode="head"
                style={[{ fontSize: 8, marginLeft: 4 }, this.props.textColor]}
              >
                {this.props.runningStatus}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', paddingVertical: 2 }}>
              <Image
                style={{
                  height: ScaleUtils.floorScale(11),
                  width: ScaleUtils.floorScale(11),
                }}
                source={ImageUtil.getImageSource('quang_duong2')}
                resizeMode="contain"
              />
              <Text
                numberOfLines={1}
                ellipsizeMode="head"
                style={{ fontSize: 8, marginLeft: 4 }}
              >
                {parseFloat(this.props.distance).toFixed(2)} km
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
            }}
          >
            <View
              style={{
                flex: 1,
              }}
            >
              <Text
                numberOfLines={1}
                ellipsizeMode="head"
                style={[{ fontSize: 11 }, this.props.textColor]}
              >
                {this.props.address}
              </Text>
            </View>
          </View>
        </View>
      );
    }
  }
}
function select(store) {
  return {
    dragonFly: store.dragonFly,
  };
}

module.exports = connect(select)(InfoRecord);
