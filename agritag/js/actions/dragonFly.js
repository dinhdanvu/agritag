import { ApiPostFormData, requestGoogleMapAPI } from './apiFetcher';
import Toast from 'react-native-root-toast';
var md5 = require('md5');
var moment = require('moment');

function showToast(
  message = 'Đã có lỗi xảy ra, vui lòng thử lại!',
  background = 'red',
  textcolor = 'white',
  position = 300
) {
  return async () => {
    {
      Toast.show(message, {
        style: { zIndex: 1000 },
        duration: Toast.durations.SHORT,
        position: position,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        backgroundColor: background,
        textColor: textcolor,
        textStyle: { fontSize: 20, fontWeight: 'bold' },
      });
    }
  };
}

function userLogin(userName, password) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      var data = {
        user_name: userName,
        password: md5(password),
        autoLogin: 'on',
      };
      dispatch(
        ApiPostFormData(
          'index.php/auth/login/loginAction',
          data,
          null,
          getState
        )
      )
        .then((response) => {
          //console.log(response)
          if (response.data.status) {
            resolve(response.data);
          } else {
            reject(response.data.mess);
          }
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  };
}

function getVehicleList() {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      var dragonFly = getState().dragonFly;
      dispatch(
        ApiPostFormData(
          'index.php/maps/loadTrackingDevicesAction',
          null,
          {
            headers: {
              authorization: `Bearer ${dragonFly.token}`,
              screen_id: 'maps',
            },
          },
          getState
        )
      )
        .then((response) => {
          if (response.data.status) {
            let vehicleList = [];
            let vehiclesLite = [];
            response.data.data.records.forEach((element) => {
              vehicleList.push({
                vehicle_name: element.vehicle_name,
                // dev_name: element.dev_name,
                driver_name: element.driver_name,
                trk_time: element.trk_time,
                latitude: parseFloat(element.latitude),
                isExpiredService: element.isExpiredService,
                dev_status: element.dev_status,
                engine: element.engine,
                speed: element.speed,
                device_type: element.device_type,
                one_day_running_time: element.one_day_running_time,
                route_time: element.route_time,
                distance_on_day: element.distance_on_day,
                address: element.address,
                number_expired: element.number_expired,
                longitude: parseFloat(element.longitude),
                plate: element.plate,
                // dev_plate: element.dev_plate,
                direction: element.direction,
                v_limit_max: parseInt(element.v_limit_max),
                driver_phone: element.driver_phone,
                satellites: element.satellites,
                battery_level: parseInt(element.battery_level),
                door_is_open: element.door_is_open,
                conditional: element.conditional,
                // map_sensor_data: JSON.parse(element.map_sensor_data),
                vehicle_id: element.vehicle_id,
                distance: element.distance,
                // move_times_on_day: element.move_times_on_day,
                stop_count_on_day: element.stop_count_on_day,
                driver_code: element.driver_code,
                over_speed_count_on_day: element.over_speed_count_on_day,
                continue_driving_time: element.continue_driving_time,
                driver_expired: element.driver_expired,
                imei: element.imei,
              });
              vehiclesLite.push({
                plate: element.plate,
                vehicle_name: element.vehicle_name,
                latitude: parseFloat(element.latitude),
                longitude: parseFloat(element.longitude),
                dev_status: element.dev_status,
                direction: element.direction,
                driver_name: element.driver_name,
              });
            });
            var vehicle = getState().vehicleTracking.vehicle;
            if (vehicle && vehicle.plate) {
              vehicle = vehicleList.find((obj) => obj.plate === vehicle.plate);
              dispatch({
                type: 'UPDATE_VEHICLE_SUCCESS',
                vehicleList,
                vehiclesLite,
                vehicle,
              });
            } else {
              dispatch({
                type: 'FETCH_VEHICLE_SUCCESS',
                vehicleList,
                vehiclesLite,
              });
            }

            resolve(true);
          } else {
            reject(response.data.mess);
          }
        })
        .catch((e) => reject(e));
    });
  };
}

function loadReviewRouteDevices(vehicle, date) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      if (date) {
        
      }
      else {
        _date = new Date();
      }
      const now = moment(_date).format('YYYY-MM-DD');
      const fromDate = now + ' ' + '0:00:00';
      const toDate = now + ' ' + '23:59:59';
      const formData = {
        vehicle_id: vehicle.vehicle_id,
        from_date: fromDate,
        to_date: toDate,
        device_type: vehicle.device_type,
        imei: vehicle.imei,
      };
      // console.log(formData);
      var dragonFly = getState().dragonFly;
      dispatch(
        ApiPostFormData(
          'index.php/maps/loadReviewRouteDevicesAction',
          formData,
          {
            headers: {
              authorization: `Bearer ${dragonFly.token}`,
              screen_id: 'maps',
            },
          },
          getState
        )
      )
        .then((response) => {
          if (response.data.status) {
            const data = response.data.data.array;
            if (data.length > 0) {
              var coordinates = [];
              var infos = [];
              data.forEach((element) => {
                coordinates.push({
                  latitude: parseFloat(element.latitude),
                  longitude: parseFloat(element.longitude),
                });
                infos.push({
                  door_is_open: vehicle.door_is_open, //
                  latitude: parseFloat(element.latitude), //
                  longitude: parseFloat(element.longitude), //
                  distance_on_day: vehicle.distance_on_day, //
                  isExpiredService: vehicle.isExpiredService, //

                  address: element.address,
                  driver_name: element.driver_name, //
                  vehicle_name: element.vehicle_name, //
                  plate: element.plate, //
                  trk_time: element.trk_time,
                  dev_status: element.dev_status, //
                  route_time: vehicle.route_time, //
                  vehicle_id: vehicle.vehicle_id, //
                  speed: element.speed, //
                  engine: element.engine,
                  satellites: element.satellites,
                  distance: element.distance,
                  v_limit_max: element.v_limit_max,
                  battery_level: element.battery_level,
                  direction: element.direction,
                  imei: vehicle.imei,
                  // continue_driving_time: element.continue_driving_time,
                });
              });
              global.coordinates = coordinates;
              global.infos = infos;
              global.vehicle = infos[0];
              // console.warn(vehicle);
              global.display = 2;

              // dispatch({
              //   type: 'LOAD_REVIEW_VEHICLE_SUCCESS',
              //   coordinates,
              //   directions,
              //   infos,
              // });
              resolve(true);
            } else {
              resolve(false);
            }
          } else {
            reject(response.data.mess);
          }
        })
        .catch((e) => reject(e));
    });
  };
}

module.exports = {
  showToast,
  userLogin,
  getVehicleList,
  loadReviewRouteDevices,
};
