/* eslint-disable dot-notation */
var API_URL = '';
// var THIRD_API_URL = '';
// var WEBHOOK_URL = '';
import langvi from '../common/langvi';
const axios = require('axios');
import linkApi from '../common/linkApi';
function requestGoogleMapAPI(url) {
          console.log(url)

  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      axios
        .get(url)
        .then((res) => {
          console.log('uuu')
          let result = res.data;
          if (result.status == 'OK') {
            resolve(result);
          } else {
            console.log('jjjj')
            reject(result.status)
          }
        })
        .catch((e) => {
          console.log('kkk')
          reject(e);
        });
    });
  };
}
function ApiGet(url, data = null) {
  return (dispatch, getState) => {
    const BASE_URL = linkApi.URL;
    if (data != null) {
      url = BASE_URL + generateURLGet(url, data);
    } else {
      url = BASE_URL;
    }
    //console.log(url);
    return new Promise((resolve, reject) => {
      axios
        .get(url)
        .then((res) => {
          let result = res.data;
          if (result.success) {
            resolve(result);
          } else {
            let error = result.error ? result.error : 'UNKNOWN_ERROR';
            let errorVi = langvi.error[error] ? langvi.error[error] : error;
            reject(errorVi);
          }
        })
        .catch((e) => {
          if (e.isAxiosError) {
            e = e.toJSON();
            e.error = e.message;
          }

          let error = e.error ? e.error : 'UNKNOWN_ERROR';
          let errorVi = langvi.error[error] ? langvi.error[error] : error;
          //console.log(e);
          reject(errorVi);
        });
    });
  };
}

function generateURLGet(url, data) {
  let text = url + '?';
  let keys = Object.keys(data);
  keys.forEach((item, idx) => {
    text += '&' + item + '=' + data[item];
  });
  return text;
}

function ApiPost(url, data) {
  return (dispatch, getState) =>
    new Promise((resolve, reject) => {
      FetchPost(url, data, getState)
        .then((response) => {
          // console.log(
          //   '===================================================Response: ',
          //   response,
          // );
          resolve(response);
        })
        .catch(async (error) => {
          if (
            error.message === 'ERR_NEED_LOGIN' ||
            error.error === 'ERR_NEED_LOGIN'
          ) {
            await reLogin(getState, dispatch, url, data, error, resolve);
          } else {
            reject(error);
          }
        });
    });
}
function ApiPostFormData(url, data, config) {
  API_URL = linkApi.URL;
  console.log(API_URL + url);
  var formData = null;
  if (data != null) {
    formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
  }
  return (dispatch, getState) =>
   new Promise((resolve, reject) => {
    axios
      .post(
        API_URL + url,
        formData,
        config
      )
      .catch((error) => {
        console.log(error, 'error');
        if (!error.response) {
          let _error = {
            success: false,
            error: langvi['ERROR_NO_INTERNET'],
          };
          reject(_error);
        } else {
          let _error = {
            success: false,
            error: json.mess,
          };
          reject(_error);
        }
      })
      .then((json) => {
        if (json) {
          if (json.status) {
            resolve(json);
          } else {
            let _error = {
              success: false,
              error: json.mess,
            };
            reject(_error);
          }
        } else {
          let _error = {
            success: false,
            error: 'UNKNOWN_ERROR',
          };
          reject(_error);
        }
      });
  });
}
function FetchPost(url, data, getState) {
  API_URL = linkApi.URL;
  //console.log(API_URL + url, data);
  return new Promise((resolve, reject) => {
    axios
      .post(API_URL + url, data)
      .then((res) => {
        return res.data;
      })
      .catch((response) => {
        return {
          success: false,
          message: response.message
            ? response.message
            : langvi['ERROR_SERVER_500'],
          error: response.error ? response.error : langvi['ERROR_SERVER_500'],
        };
      })
      .then((json) => {
        if (json.success) {
          resolve(json);
        } else {
          let _error = {
            success: false,
            error: json.error
              ? langvi['error'][json.error]
                ? langvi['error'][json.error]
                : json.error
              : langvi['error'][json.message]
              ? langvi['error'][json.message]
              : json.message,
            message: json.message
              ? langvi['error'][json.message]
                ? langvi['error'][json.message]
                : json.message
              : langvi['error'][json.error]
              ? langvi['error'][json.error]
              : json.error,
          };
          //console.log('apifetcher reject', _error, data);
          reject(_error);
        }
      });
  });
}

function FetchPostFormData(url, data, config, getState) {
  API_URL = linkApi.URL;
  console.log(API_URL + url);
  var formData = null;
  if (data != null) {
    formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
  }
  return new Promise((resolve, reject) => {
    axios
      .post(
        API_URL = linkApi.URL,
        formData,
        config
      )
      .catch((error) => {
        console.log(error, '??????????????ERO');
        if (!error.response) {
          let _error = {
            success: false,
            error: langvi['ERROR_NO_INTERNET'],
          };
          reject(_error);
        } else {
          let _error = {
            success: false,
            error: json.mess,
          };
          reject(_error);
        }
      })
      .then((json) => {
        if (json) {
          if (json.status) {
            resolve(json);
          } else {
            let _error = {
              success: false,
              error: json.mess,
            };
            reject(_error);
          }
        } else {
          let _error = {
            success: false,
            error: 'UNKNOWN_ERROR',
          };
          reject(_error);
        }
      });
  });
}

module.exports = {ApiGet, ApiPost, ApiPostFormData, requestGoogleMapAPI};
