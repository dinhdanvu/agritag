import socketio

# standard Python
sio = socketio.Client()


@sio.on('steer')
def on_message(data):
    print('I received a message!', data)


sio.connect('http://localhost:4567')
