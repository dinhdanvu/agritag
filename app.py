import Jetson.GPIO as GPIO
import time 
from adafruit_servokit import ServoKit
import board
import busio
from pyPS4Controller.controller import Controller

pin11 = 'UART2_RTS'
pin12 = 'DAP4_SCLK'
pin15 = 'LCD_TE'
pin16 = 'SPI2_CS1'

pin21 = 'SPI1_MISO'
pin22 = 'SPI2_MISO'
pin23 = 'SPI1_SCK'
pin24 = 'SPI1_CS0'

_angle = 0

GPIO.setwarnings(False)
GPIO.setup(pin11, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(pin12, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(pin15, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(pin16, GPIO.OUT, initial=GPIO.LOW) 

GPIO.setup(pin21, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(pin22, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(pin23, GPIO.OUT, initial=GPIO.LOW) 
GPIO.setup(pin24, GPIO.OUT, initial=GPIO.LOW) 
print("Initializing Servos")
i2c_bus0=(busio.I2C(board.SCL_1, board.SDA_1))
print("Initializing ServoKit")
kit = ServoKit(channels=16, i2c=i2c_bus0)
# kit[0] is the bottom servo
# kit[1] is the top servo
print("Done initializing")
def first_forward():
  GPIO.output(pin11, 0)
  GPIO.output(pin12, 1)

def first_back():
  GPIO.output(pin11, 1)
  GPIO.output(pin12, 0)

def second_forward():
  GPIO.output(pin15, 1)
  GPIO.output(pin16, 0)

def second_back():
  GPIO.output(pin15, 0)
  GPIO.output(pin16, 1)

def third_forward():
  GPIO.output(pin21, 1)
  GPIO.output(pin22, 0)

def third_back():
  GPIO.output(pin21, 0)
  GPIO.output(pin22, 1)

def fourth_back():
  GPIO.output(pin23, 1)
  GPIO.output(pin24, 0)

def fourth_forward():
  GPIO.output(pin23, 0)
  GPIO.output(pin24, 1)
def turnRight():
  first_back()
  second_forward()
  third_back()
  fourth_forward()
def turnLeft():
  first_forward()
  second_back()
  third_forward()
  fourth_back()
def allForward():
  first_forward()
  second_forward()
  third_forward()
  fourth_forward()
def allBack():
  first_back()
  second_back()
  third_back()
  fourth_back()
def all_stop():
  GPIO.output(pin11, 0)
  GPIO.output(pin12, 0)
  GPIO.output(pin15, 0)
  GPIO.output(pin16, 0)
  GPIO.output(pin21, 0)
  GPIO.output(pin22, 0)
  GPIO.output(pin23, 0)
  GPIO.output(pin24, 0)
def center():
   kit.servo[0].angle = 120
   kit.servo[1].angle = 120
   kit.servo[2].angle = 90
   kit.servo[3].angle = 80
   global _angle
   _angle = 0

def turnAround():
   kit.servo[0].angle = 120 + 50
   kit.servo[1].angle = 120 - 50
   kit.servo[2].angle = 90 - 50
   kit.servo[3].angle = 80 + 50

def rotateRight():
   global _angle
   if _angle < 87:
     _angle += 3
   kit.servo[0].angle = 120 - _angle
   kit.servo[1].angle = 120 - _angle
   kit.servo[2].angle = 90 + _angle
   kit.servo[3].angle = 80 + _angle
def rotateLeft():
   global _angle
   if _angle > -77:
     _angle -= 3
   kit.servo[0].angle = 120 - _angle
   kit.servo[1].angle = 120 - _angle
   kit.servo[2].angle = 90 + _angle
   kit.servo[3].angle = 80 + _angle
kit.servo[0].actuation_range = 270
kit.servo[0].set_pulse_width_range(1200, 3330)
kit.servo[1].actuation_range = 270
kit.servo[1].set_pulse_width_range(1200, 3330)
center()
class MyController(Controller):
   
    def __init__(self, **kwargs):
        Controller.__init__(self, **kwargs)
    def on_x_press(self):
       all_stop()
    def on_square_press(self):
       all_stop()
    def on_circle_press(self):
       all_stop()
    def on_triangle_press(self):
       all_stop()
    def on_R3_up(self, a):
       rotateLeft()
    def on_R2_release(self):
       all_stop()
       time.sleep(0.5)
       allBack()
    def on_R3_y_at_rest(self):
       return
    def on_circle_release(self):
       return
    def on_x_release(self):
       return
    def on_triangle_release(self):
       return
    def on_R3_down(self, a):
       rotateRight()
    def on_R2_press(self, a):
      if a > 0:
         all_stop()
         time.sleep(0.5)
         allForward()
   #  def on_R1_press(self):
   #     turnAround()
    def on_share_press(self):
       all_stop()
       time.sleep(0.5)
       turnAround()
       time.sleep(0.5)
       turnLeft()
    def on_options_press(self):
       all_stop()
       time.sleep(0.5)
       turnAround()
       time.sleep(0.5)
       turnRight()
    def on_L1_press(self):
       center()

controller = MyController(interface="/dev/input/js0", connecting_using_ds4drv=False)
controller.debug = False
controller.listen()